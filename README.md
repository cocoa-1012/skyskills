# Environment preferences
node version 12.21.0

# Start project
1. yarn install
2. yarn start
3. open http://localhost:3000

# Run tests
1. yarn test

# Build project
1. yarn build

# Before commit
Before committing, you need to check that the changed code matches the accepted style guide. The project provides automatic code validation with .eslint and style code fixes with .prettier.
Before first commit you need to setup pre-commit hook. Follow the next steps:
1. npx husky install
2. npx husky add .husky/pre-commit "yarn lint-staged"

