const path = require('path');

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials', '@storybook/preset-create-react-app'],
  webpackFinal: async config => ({
    ...config,
    resolve: {
      ...config.resolve,
      alias: {
        ...config.resolve?.alias,
        '@providers': path.resolve('src/providers/'),
        '@components': path.resolve('src/components/'),
        '@constants': path.resolve('src/constants/'),
        '@hooks': path.resolve('src/hooks/'),
        '@pages': path.resolve('src/pages/'),
        '@styles': path.resolve('src/styles/'),
        '@icons': path.resolve('src/icons/'),
        '@utils': path.resolve('src/utils/'),
      },
    },
  }),
};
