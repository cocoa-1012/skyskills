import {RecoilValueReadOnly, useRecoilValueLoadable} from "recoil";

type TLoadable = {
  loading: boolean;
  error: Error | undefined;
  data: any;
}

const useLoadable = <T extends {}>(state: RecoilValueReadOnly<T>, defaultValue?: any): TLoadable => {
  const loadable = useRecoilValueLoadable(state);

  return {
    loading: loadable.state === 'loading',
    error: loadable.state === 'hasError' ? loadable.contents : undefined,
    data: loadable.state === 'hasValue' ? loadable.contents : defaultValue,
  };
};

export default useLoadable;
