import { TChannel } from '@sharedTypes/channel';
import {useEffect, useState} from 'react';
import {useRecoilState, useRecoilValue} from 'recoil';
import { channelsState, messagesState, alertsState, userState, filesState } from '@state';
import { useLoadable } from '@hooks';
import { MESSAGE_AREA_TEMPLATES } from './Messages.data';

const useMessages = () => {
  const [selectedChannel, setSelectedChannel] = useState<TChannel>();
  const [alerts, setAlert] = useRecoilState(alertsState);
  const user = useRecoilValue(userState);
  const files = useRecoilValue(filesState);

  const {
    data: channels,
    error: channelsError,
    loading: channelsLoading
  } = useLoadable(channelsState([]), []);

  const {
    data: messages,
    error: messagesError,
    loading: messagesLoading
  } = useLoadable(messagesState(selectedChannel?.uuid), []);

  useEffect(() => {
    if (channelsError) {
      // Add error messages to Alert state
      setAlert([...alerts, channelsError.message])
    }

    if (channelsError) {
      // Add error messages to Alert state
      setAlert([...alerts, channelsError.message])
    }
  }, [channelsError, messagesError]);


  const onChannelSelect = (selected: TChannel) => {
    setSelectedChannel(selected);
  };

  return {
    channels,
    selectedChannel,
    allChannels: channels,
    files,
    user,
    messages,
    messageAreaTemplates: MESSAGE_AREA_TEMPLATES,
    channelsLoading,
    messagesLoading,
    onChannelSelect,
  };
};

export default useMessages;
