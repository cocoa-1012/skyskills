export const MESSAGE_AREA_TEMPLATES = [
  {
    category: 'Driver Details',
    templates: [
      {
        label: '{First Name}',
        value: 'Joe',
      },
      {
        label: '{Last Name}',
        value: 'Kohorst',
      },
      {
        label: '{Phone}',
        value: '(359) 1234 23',
      },
      {
        label: '{Email}',
        value: 'test@gmail.com',
      },
      {
        label: '{City}',
        value: 'Austin',
      },
      {
        label: '{State}',
        value: 'Texas',
      },
      {
        label: '{Current Location}',
        value: 'Morningside-Lenox Park',
      },
    ],
  },
  {
    category: 'Load Details',
    templates: [
      {
        label: '{Current Load #}',
        value: 'Normal',
      },
      {
        label: '{Destination}',
        value: '20532',
      },
    ],
  },
  {
    category: 'My Details',
    templates: [
      {
        label: '{My First Name}',
        value: 'Cristiano',
      },
      {
        label: '{My Lsat Name}',
        value: 'Ronaldo',
      },
      {
        label: '{My Phone}',
        value: '(359) 1234 23',
      },
      {
        label: '{My Email}',
        value: 'ronaldo@gmail.com',
      },
    ],
  },
];
