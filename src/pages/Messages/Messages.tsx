import React from 'react';
import {NAVIGATION_LINKS} from '@constants';
import {
  Navigation,
  Flex,
  Header,
  Sidebar,
  ChannelsHeader,
  PropertyPanel,
  ChannelsFilter,
  ChannelsList,
  Chat, ChannelsSidebar, Loader,
} from '@components';
import {ChatIcon} from "@icons";

import useMessages from '@pages/Messages/useMessages';
import styles from './Messages.module.scss';

const Messages = () => {
  const {
    user,
    files,
    channels,
    selectedChannel,
    messages,
    messageAreaTemplates,
    allChannels,
    channelsLoading,
    messagesLoading,
    onChannelSelect,
  } = useMessages();

  return (
    <Flex fullHeight className={styles.container}>
      <Navigation topMenu={NAVIGATION_LINKS.top} bottomMenu={NAVIGATION_LINKS.bottom}/>

      <Flex>
        <Sidebar className={styles.leftSidebar}>
          <ChannelsHeader
            className={styles.channelsHeader}
            rate={0}
            username={user.username}
            telephone={user.telephone}
            textTemplates={messageAreaTemplates}
          />
          <div className={styles.channelsBar}>
            <ChannelsFilter
              options={allChannels}
              className={styles.channelsHeader}
            />
            <ChannelsList
              activeChannel={selectedChannel}
              isLoading={channelsLoading}
              userUUID={user.uuid}
              className={styles.channelsList}
              channels={channels}
              onChannelSelect={onChannelSelect}
            />

          </div>
        </Sidebar>

        {
          (selectedChannel && !messagesLoading) && (
            <Flex column>
              <Header className={styles.header}>
                <PropertyPanel follower={2} name={selectedChannel?.name} phone={selectedChannel?.telephone}/>
              </Header>

              <Flex row fullHeight className={styles.content}>
                <Chat
                  userUUID={user.uuid}
                  messages={messages}
                  textTemplates={messageAreaTemplates}
                  scheduledMessagesCount={user.scheduledMessages.length}
                  lastScheduledMessage={user.scheduledMessages[user.scheduledMessages.length - 1]}
                  onMessageSend={() => {
                  }}
                />
                {
                  selectedChannel && (
                    <Sidebar
                      className={styles.rightSidebar}>
                      <ChannelsSidebar channel={selectedChannel} files={files}/>
                    </Sidebar>
                  )
                }
              </Flex>
            </Flex>
          )
        }

        {
          (!selectedChannel && !messagesLoading) && (
            <Flex className={styles.noChatScreen} verticalCenter horizontalCenter column>
              <Flex column verticalCenter horizontalCenter className={styles.noChatIcon}>
                <ChatIcon/>
                <p>Nothing is selected. Please use the list to the left to select a conversation.</p>
              </Flex>
            </Flex>
          )
        }

        {
          messagesLoading && (
            <Flex className={styles.noChatScreen} verticalCenter horizontalCenter column>
              <Loader isVisible />
            </Flex>
          )
        }

      </Flex>
    </Flex>
  );
};

export default Messages;
