import React from 'react';
import {SignInForm} from '@forms';
import styles from '@pages/SignIn/Signin.module.scss';

const SignIn = () => {
  return (
    <div className={styles.container}>
      <SignInForm
        isSaving={false}
        isLoading={false}
        onError={() => {}}
        onSubmit={() => {}}
        initialValues={{
          userName: '',
          password: '',
        }}
      />
    </div>
  );
};

export default SignIn;
