import React from 'react';
import { SettingsGeneralForm, SettingsSecurityForm } from '@forms';
import { Navigation, MenuList, MenuItem, Flex, Header } from '@components';
import { NAVIGATION_LINKS } from '@constants';
import styles from './Settings.module.scss';
import useSettings from './useSettings';

const Settings = () => {
  const { activeTab, setActiveTab } = useSettings();

  return (
    <Flex fullHeight className={styles.container}>
      <Navigation topMenu={NAVIGATION_LINKS.top} bottomMenu={NAVIGATION_LINKS.bottom} />

      <Flex>
        <Flex column>
          <Header>
            <h5>Settings</h5>
          </Header>

          <Flex>
            <div className={styles.formContainer}>
              <div className={styles.navigation}>
                <MenuList position="vertical">
                  <MenuItem isActive={activeTab === 'general'} onClick={() => setActiveTab('general')}>
                    General Settings
                  </MenuItem>
                  <MenuItem isActive={activeTab === 'security'} onClick={() => setActiveTab('security')}>
                    Security Settings
                  </MenuItem>
                </MenuList>
              </div>

              {activeTab === 'general' && (
                <div className={styles.form}>
                  <h3>General Settings</h3>
                  <SettingsGeneralForm
                    isSaving={false}
                    isLoading={false}
                    onError={errors => console.log('SettingsGeneralForm error', errors)}
                    onSubmit={data => console.log('SettingsGeneralForm onSubmit', data)}
                    initialValues={{
                      firstName: 'Marc',
                      lastName: 'Brown',
                      timezone: 'China Taiwan Time',
                      telephone: '(321) 722-1232',
                    }}
                  />
                </div>
              )}

              {activeTab === 'security' && (
                <div className={styles.form}>
                  <h3>Security Settings</h3>
                  <SettingsSecurityForm
                    isSaving={false}
                    isLoading={false}
                    onError={errors => console.log('SettingsSecurityForm error', errors)}
                    onSubmit={data => console.log('SettingsSecurityForm onSubmit', data)}
                    initialValues={{
                      password: '123456',
                      securePhone: '(321) 722-1232',
                      backupEmail: 'backup@gmail.ccom',
                    }}
                  />
                </div>
              )}
            </div>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Settings;
