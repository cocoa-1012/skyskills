import { useState } from 'react';

const useSettings = () => {
  const [activeTab, setActiveTab] = useState('general');

  return {
    activeTab,
    setActiveTab,
  };
};

export default useSettings;
