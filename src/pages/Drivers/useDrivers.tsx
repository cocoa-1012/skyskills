import {useEffect, useState} from 'react';
import { TRow } from '@components/Table/Table.types';
import { columns } from '@pages/Drivers/Drivers.data';
import { TChannel } from '@sharedTypes/channel';
import { TFormData } from '@sharedTypes/forms';
import { channelsState, alertsState } from "@state";
import {useLoadable} from "@hooks";
import {useRecoilState} from "recoil";

const useDrivers = () => {
  const [alerts, setAlert] = useRecoilState(alertsState);

  const {
    data: drivers,
    error: driversError,
    loading: driversLoading
  } = useLoadable(channelsState(['Drivers']), []);

  const [selectedDriver, setSelectedDriver] = useState<TChannel | undefined>();
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [isLoading] = useState<boolean>(false);
  const [isSaving] = useState<boolean>(false);

  useEffect(() => {
    if (driversError) {
      // Add error messages to Alert state
      setAlert([...alerts, driversError.message])
    }
  }, [driversError]);


  const onRowSelect = (row: TRow) => {
    setIsEditing(false);
    const selected = drivers.find((driver: TChannel) => driver.uuid === row.id);
    setSelectedDriver(selected);
  };

  const updateDriver = (data: TFormData) => {
    console.log('Update driver', data);
  };

  return {
    drivers,
    driversLoading,
    selectedDriver,
    onRowSelect,
    columns,
    isEditing,
    isLoading,
    isSaving,
    setIsEditing,
    updateDriver,
  };
};

export default useDrivers;
