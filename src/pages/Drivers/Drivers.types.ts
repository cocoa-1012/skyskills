import { TChannel } from '@sharedTypes/channel';
import { TFormData } from '@sharedTypes/forms';

export type TDriversDetails = {
  driver: TChannel;
  isLoading: boolean;
  isSaving: boolean;
  isEditing: boolean;
  onSave: (data: TFormData) => void;
  onClose: () => void;
};
