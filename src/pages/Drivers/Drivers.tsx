import React from 'react';
import { Table, Input, Navigation, Flex, Header, Sidebar, Content, Loader } from '@components';
import { SearchIcon } from '@icons';
import { TChannel } from '@sharedTypes/channel';

import { NAVIGATION_LINKS } from '@constants';
import DriverDetails from './Driver.details';
import styles from './Drivers.module.scss';
import useDrivers from './useDrivers';

const Drivers = () => {
  const {
    drivers,
    driversLoading,
    columns,
    isSaving,
    isLoading,
    isEditing,
    setIsEditing,
    selectedDriver,
    onRowSelect,
    updateDriver,
  } = useDrivers();

  return (
    <Flex fullHeight className={styles.container}>
      <Navigation topMenu={NAVIGATION_LINKS.top} bottomMenu={NAVIGATION_LINKS.bottom} />

      <Loader isVisible={driversLoading} />

      {!driversLoading && (
        <Flex>
          <Flex column>
            <Header>
              <h5>Drivers</h5>
              <div className={styles.search}>
                <Input
                  fullWidth
                  inputSize="small"
                  placeholder="Search drivers"
                  iconLeft={<SearchIcon />}
                  className={styles.search}
                  onChange={() => {}}
                />
              </div>
            </Header>

            <Content>
              <Table
                columns={columns}
                data={drivers.map((row: TChannel) => ({
                  id: row.uuid,
                  name: row.name,
                  group: row.groups.join(', '),
                  phone: row.telephone,
                  labels: row.labels,
                }))}
                onRowSelect={row => onRowSelect(row)}
                emptyMessage="No Record Found"
                isLoading={false}
              />
            </Content>
          </Flex>

          {selectedDriver && (
            <Sidebar className={styles.sidebar}>
              <DriverDetails
                isEditing={isEditing}
                isSaving={isSaving}
                isLoading={isLoading}
                driver={selectedDriver}
                onSave={updateDriver}
                onClose={() => setIsEditing(!isEditing)}
              />
            </Sidebar>
          )}
        </Flex>
      )}
    </Flex>
  );
};

export default Drivers;
