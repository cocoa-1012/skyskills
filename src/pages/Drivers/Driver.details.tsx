import React from 'react';
import { ChannelsDetails, ChannelsNotes, ActionButton, Button, Flex } from '@components';
import { NewMessageIcon, PencilAlternateIcon } from '@icons';
import { ChannelDetailsForm } from '@forms';
import { TDriversDetails } from './Drivers.types';
import styles from './Drivers.module.scss';

const DriverDetails = ({ driver, isLoading, isEditing, onClose, isSaving, onSave }: TDriversDetails) => {
  return (
    <Flex column fullHeight>
      {!isEditing && (
        <>
          <Flex className={styles.channelHeader}>
            <h4>{driver?.name}</h4>
            <div>
              <Button onClick={() => {}} size="small" icon={<NewMessageIcon />} color="success" variant="contained" />

              <ActionButton size="small" icon={<PencilAlternateIcon />} onClick={onClose} />
            </div>
          </Flex>

          <ChannelsDetails className={styles.details} channel={driver} />

          <ChannelsNotes placeholder="You can enter some notes here (visible only to you) ..." />
        </>
      )}

      {isEditing && (
        <ChannelDetailsForm
          isLoading={isSaving}
          isSaving={isLoading}
          onSubmit={onSave}
          onCancel={onClose}
          initialValues={{
            name: driver.name,
            groups: driver.groups,
            labels: driver.labels?.map(group => ({ label: group, value: group })),
            phone: driver.telephone,
            email: driver.email,
            localTime: driver.localTime,
            currentLocation: driver.currentLocation.address,
            deliveryTime: driver.deliveryTime,
            homeAddress: driver.location.address,
          }}
        />
      )}
    </Flex>
  );
};

export default DriverDetails;
