import {useEffect} from "react";
import {useRecoilState} from "recoil";
import {alertsState, channelsState} from "@state";
import {useLoadable} from "@hooks";
import {TChannel} from "@sharedTypes/channel";
import { columns } from "./Users.data";

const useUsers = () => {
  const [alerts, setAlert] = useRecoilState(alertsState);

  const {
    data: users,
    error: usersError,
    loading: usersLoading
  } = useLoadable<TChannel[]>(channelsState([]), []);


  useEffect(() => {
    if (usersError) {
      // Add error messages to Alert state
      setAlert([...alerts, usersError.message])
    }
  }, [usersError]);

  return {
    columns,
    users,
    usersLoading
  };
};

export default useUsers;
