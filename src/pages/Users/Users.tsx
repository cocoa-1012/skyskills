import React from 'react';
import { Header, Navigation, Flex, Input, Table, Content, Loader } from '@components';
import { NAVIGATION_LINKS } from '@constants';
import { SearchIcon } from '@icons';
import useUsers from '@pages/Users/useUsers';
import { TChannel } from '@sharedTypes/channel';
import styles from './Users.module.scss';

const Users = () => {
  const { users, usersLoading, columns } = useUsers();

  return (
    <Flex fullHeight className={styles.container}>
      <Navigation topMenu={NAVIGATION_LINKS.top} bottomMenu={NAVIGATION_LINKS.bottom} />

      <Loader isVisible={usersLoading} />

      {!usersLoading && (
        <Flex>
          <Flex column>
            <Header>
              <h5>Users</h5>
              <div className={styles.search}>
                <Input
                  fullWidth
                  placeholder="Search users"
                  iconLeft={<SearchIcon />}
                  inputSize="small"
                  onChange={() => {}}
                />
              </div>
            </Header>

            <Content>
              <Table
                columns={columns}
                data={users.map((row: TChannel) => ({
                  id: row.uuid,
                  name: row.name,
                  group: row.groups.join(', '),
                  phone: row.telephone,
                  labels: row.labels,
                }))}
                emptyMessage="No Record Found"
                isLoading={false}
              />
            </Content>
          </Flex>
        </Flex>
      )}
    </Flex>
  );
};

export default Users;
