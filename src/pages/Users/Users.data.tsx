export const columns = [
  {
    title: 'Name',
    field: 'name',
    type: 'text',
    flex: '0 0 160px',
  },
  {
    title: 'Group',
    field: 'group',
    type: 'text',
    flex: '0 0 160px',
  },
  {
    title: 'Phone',
    field: 'phone',
    type: 'text',
    flex: '0 0 160px',
  },
  {
    title: 'Label',
    field: 'labels',
    type: 'tags',
  },
];
