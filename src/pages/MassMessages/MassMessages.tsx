import React from 'react';
import { Flex, Header, Navigation } from '@components';
import { NAVIGATION_LINKS } from '@constants';
import styles from './MassMessages.module.scss';

const MassMessages = () => {
  return (
    <Flex fullHeight>
      <Navigation topMenu={NAVIGATION_LINKS.top} bottomMenu={NAVIGATION_LINKS.bottom} />

      <Flex>
        <Flex column>
          <Header>
            <h5>Mass messages</h5>
          </Header>

          <Flex className={styles.container}>
            <h2>Mass Messages</h2>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default MassMessages;
