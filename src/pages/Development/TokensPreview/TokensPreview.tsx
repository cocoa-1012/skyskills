import React, { ChangeEvent, useState } from 'react';
import { getInitials } from '@utils';
import {
  ActionButton,
  Button,
  Counter,
  Input,
  Tags,
  TextArea,
  Checkbox,
  Radio,
  MenuItem,
  MenuList,
  Notification,
  Avatar,
  Message,
  MessageArea,
  Dropdown,
  UploadFileSimple,
  FilePreview,
  InputSecure,
  Modal,
} from '@components';
import { TSimpleFile } from '@components/UploadFile/UploadFile.types';
import { FilterIcon, ImageIcon, SearchIcon } from '@icons';
import { TTagItem } from '@components/Tag/Tag.types';
import styles from './TokensPreview.module.scss';

const RADIOS_MOCK = [
  {
    id: 'e5e3a5bc-de0a-4285-b077-04f0c013bdd5',
    label: 'Option 1',
    value: 'Option value #1',
  },
  {
    id: 'e5e3a5bc-de0a-4285-b077-04f0c013bdd6',
    label: 'Option 2',
    value: 'Option value #2',
  },
  {
    id: 'e5e3a5bc-de0a-4285-b077-04f0c013bdd7',
    label: 'Option 3',
    value: 'Option value #3',
  },
];

const TokensPreview = () => {
  const [tags, setTags] = useState<Array<TTagItem>>([]);
  const [files, setFiles] = useState<any>([]);
  const [selectedRadio, setSelectedRadio] = useState<string>('');
  const [selectedCheckboxes, setSelectedCheckboxes] = useState<Array<string>>([]);
  const [openModal, setOpenModal] = useState<boolean>(false);

  const onAddTag = (data: TTagItem) => {
    setTags([
      ...tags,
      {
        ...data,
        icon: <FilterIcon />,
      },
    ]);
  };

  const onRemoveTag = (data: TTagItem) => {
    const filteredTags = tags.filter((tag: TTagItem) => tag.value !== data.value);
    setTags(filteredTags);
  };

  const onCancelModal = () => {
    setOpenModal(false);
  };

  return (
    <div className={styles.tokensPreview}>
      <h1>Tokens Preview</h1>

      <h3>Inputs</h3>

      <Input fullWidth iconLeft={<SearchIcon />} id="input-example-id" inputSize="small" placeholder="Some text..." />

      <h3>Modal</h3>
      <Button onClick={() => setOpenModal(true)} color="success" variant="contained">
        Modal
      </Button>
      <Modal
        isVisible={openModal}
        title="Modal Title"
        cancelButton={
          <Button color="danger" variant="contained" onClick={() => setOpenModal(false)}>
            Cancel
          </Button>
        }
        successButton={
          <Button color="success" variant="contained" onClick={() => setOpenModal(false)}>
            Submit
          </Button>
        }
        onCancel={onCancelModal}
      >
        <div className={styles.modalBody}>Modal Body</div>
      </Modal>

      <h3>Buttons</h3>

      <Button color="success" variant="contained">
        Test
      </Button>

      <ActionButton icon={<FilterIcon />}>Test</ActionButton>

      <Counter min={0} max={10} />

      <h3>Tags</h3>

      <Tags
        icon={<SearchIcon />}
        tags={tags}
        onRemoveTag={onRemoveTag}
        onAddTag={onAddTag}
        placeholder="Placeholder text..."
      />

      <TextArea
        fullWidth
        placeholder="Some text..."
        style={{
          height: 200,
        }}
      />

      <h3>Checkbox</h3>
      {RADIOS_MOCK.map(({ id, label, value }) => (
        <Checkbox
          id={`checkbox-${id}`}
          key={`checkbox-${id}`}
          label={label}
          value={value}
          onChange={({ target }: ChangeEvent<HTMLInputElement>) => {
            if (selectedCheckboxes.includes(target.value)) {
              setSelectedCheckboxes(selectedCheckboxes.filter(option => option !== target.value));
            } else {
              setSelectedCheckboxes([...selectedCheckboxes, target.value]);
            }
          }}
        />
      ))}

      <div>
        Chosen checkbox options: <strong>{selectedCheckboxes.join(', ')}</strong>
      </div>

      <h3>Radio</h3>

      {RADIOS_MOCK.map(({ id, label, value }) => (
        <Radio
          id={`radio-${id}`}
          key={`radio-${id}`}
          label={label}
          value={value}
          checked={value === selectedRadio}
          onChange={({ target }: ChangeEvent<HTMLInputElement>) => {
            setSelectedRadio(target.value);
          }}
        />
      ))}

      <div>
        Chosen radio option: <strong>{selectedRadio}</strong>
      </div>

      <Button
        color="success"
        variant="contained"
        dropdown={
          <MenuList>
            <MenuItem size="large" color="primary">
              John Koslow
            </MenuItem>
            <MenuItem size="large" color="primary">
              John Koslow
            </MenuItem>
            <MenuItem size="large" color="primary">
              John Koslow
            </MenuItem>
            <MenuItem size="large" color="primary">
              John Koslow
            </MenuItem>
            <MenuItem size="large" color="primary">
              John Koslow
            </MenuItem>
            <MenuItem size="large" color="primary">
              John Koslow
            </MenuItem>
            <MenuItem size="large" color="primary">
              John Koslow
            </MenuItem>
          </MenuList>
        }
      >
        Dropdown Menu
      </Button>

      <h3>Avatars</h3>

      <div className={styles.avatarsContainer}>
        <Avatar size="large">{getInitials('John Koslow')}</Avatar>

        <Avatar>{getInitials('John Koslow')}</Avatar>

        <Avatar size="small">{getInitials('John Koslow')}</Avatar>
      </div>

      <h3>Notifications</h3>

      <Notification
        leftComponent={<Avatar>{getInitials('John Koslow')}</Avatar>}
        rightComponent={<Button>View</Button>}
        title="Joe Kohorst"
        content="just wrote you a message"
      />

      <h3>Messages</h3>

      <div className={styles.messagesContainer}>
        <Message
          uuid="567"
          status="sent"
          isMass={false}
          text="Some message"
          from={{
            uuid: '234',
            username: 'John Doe',
          }}
          isOwner
          creationDateTime={new Date().toString()}
        />

        <Message
          uuid="678"
          status="pending"
          text="Some message"
          isMass={false}
          from={{
            uuid: '123',
            username: 'John Doe',
          }}
          isOwner={false}
          creationDateTime={new Date().toString()}
        />
      </div>

      <h3>Send new message</h3>
      <div>
        <MessageArea
          textTemplates={[]}
          onTextChange={() => {}}
          placeholder="Write a message..."
          className={styles.messageArea}
        />
      </div>

      <h3>Simple dropdown</h3>

      <Dropdown content={<div>Any content</div>}>Any trigger</Dropdown>

      <h3>Uploading</h3>

      {files?.map((file: TSimpleFile) => (
        <FilePreview
          onRemove={() => {
            setFiles(files.filter((f: TSimpleFile) => f?.file?.name !== file.file.name));
          }}
          detailed={false}
          type="image"
          image={file.link}
        />
      ))}

      <UploadFileSimple
        onFileUpload={file => {
          setFiles([...files, file]);
        }}
      >
        <ImageIcon />
      </UploadFileSimple>

      <h3>Secure Input</h3>

      <InputSecure label="Passsword" value="qwerty" className={styles.secureInput}>
        <p>
          Your current password is: <strong>Strong</strong>
        </p>
      </InputSecure>
    </div>
  );
};

export default TokensPreview;
