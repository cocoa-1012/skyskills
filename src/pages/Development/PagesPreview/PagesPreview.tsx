import React from 'react';
import styles from './PagesPreview.module.scss';

const PagesPreview = () => {
  return (
    <div className={styles.componentsPreview}>
      <h1>Pages Preview</h1>
    </div>
  );
};

export default PagesPreview;
