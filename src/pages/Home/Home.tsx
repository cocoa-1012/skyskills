import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTES } from '@constants';
import styles from './Home.module.scss';

const Home = () => {
  return (
    <div className={styles.homeContainer}>
      <h1>Logytext</h1>
      <nav className={styles.menu}>
        <Link to={ROUTES.messages}>App</Link>
        <Link to={ROUTES.development.figmaTokens}>Tokens</Link>
        <Link to={ROUTES.development.figmaPages}>Pages</Link>
        <Link to={ROUTES.development.signinPage}>SignIn</Link>
      </nav>
    </div>
  );
};

export default Home;
