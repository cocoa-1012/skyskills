export const validators = {
  userName: {
    required: 'Username is required',
  },
  password: {
    required: 'Password is required',
  },
};
