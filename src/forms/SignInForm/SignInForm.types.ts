import {IStandardFormProps} from '@sharedTypes/forms';

export type TSignInForm = IStandardFormProps & {
  initialValues: {
    userName: string;
    password: string;
  };
};
