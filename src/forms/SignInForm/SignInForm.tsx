import React from 'react';
import clsx from 'clsx';
import {useForm} from 'react-hook-form';
import {FormContainer} from '@forms';
import {Button, Input} from '@components';
import {LogoIcon} from "@icons";
import {TSignInForm} from './SignInForm.types';
import styles from './SignInForm.module.scss';
import {validators} from './SignInForm.data';

const SignInForm = ({isLoading, isSaving, initialValues, onError, onSubmit}: TSignInForm) => {
  const {
    register,
    handleSubmit,
    formState: {errors, isDirty},
  } = useForm();
  return (
    <FormContainer
      {...{
        isLoading,
        isSaving,
      }}
      className={styles.container}
    >
      <div className={styles.titleWrap}>
        <h3>
          <LogoIcon/>
          <span>Company Name</span>
        </h3>
        <h5>You have been unlogged</h5>
        <p>Log in again to continue</p>
      </div>

      <Input
        className={styles.filed}
        label="Username"
        defaultValue={initialValues?.userName}
        placeholder="Add username..."
        error={errors?.userName?.message}
        {...register('userName', validators.userName)}
      />

      <Input
        className={clsx(styles.filed, styles.inputSpace)}
        label="Password"
        defaultValue={initialValues?.password}
        placeholder="Add password..."
        error={errors?.password?.message}
        {...register('password', validators.password)}
      />

      <Button
        className={styles.sendButton}
        fullWidth
        type="button"
        variant="contained"
        color="success"
        onClick={() => handleSubmit(onSubmit, onError)()}
        disabled={!isDirty}
      >
        Send
      </Button>

      <div className={styles.signUp}>
        <p>Don&apos;t have an account? <a href="#sign">Sign Up</a></p>
        <a href="#password">Forgotten password?</a>
      </div>
    </FormContainer>
  );
};

export default SignInForm;
