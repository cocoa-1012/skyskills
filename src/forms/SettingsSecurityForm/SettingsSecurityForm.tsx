import React from 'react';
import { FormContainer } from '@forms';
import { Button, InputSecure } from '@components';
import { useForm } from 'react-hook-form';
import { TSettingsSecurityForm } from './SettingsSecurityForm.types';
import { validators } from './SettingsSecurityForm.data';
import styles from './SettingsSecurityForm.module.scss';

const SettingsSecurityForm = ({ isLoading, isSaving, onError, onSubmit, initialValues }: TSettingsSecurityForm) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isDirty },
  } = useForm();
  return (
    <FormContainer
      {...{
        isLoading,
        isSaving,
      }}
      className={styles.container}
    >
      <InputSecure
        className={styles.filed}
        label="Password"
        defaultValue={initialValues?.password}
        placeholder="Add surname..."
        error={errors?.password?.message}
        {...register('password', validators.password)}
      >
        <p>
          Your current password is: <strong>Strong</strong>
        </p>
      </InputSecure>

      <InputSecure
        className={styles.filed}
        label="Secure Phone"
        defaultValue={initialValues?.securePhone}
        placeholder="Add timezone..."
        error={errors?.securePhone?.message}
        {...register('securePhone', validators.securePhone)}
      >
        (321) ***-1232
      </InputSecure>

      <InputSecure
        className={styles.filed}
        label="Backup Email"
        defaultValue={initialValues?.backupEmail}
        placeholder="Add phone number..."
        error={errors?.backupEmail?.message}
        {...register('backupEmail', validators.backupEmail)}
      >
        marc@logytext.com
      </InputSecure>

      <Button
        className={styles.sendButton}
        variant="contained"
        color="success"
        onClick={() => handleSubmit(onSubmit, onError)()}
        disabled={!isDirty}
      >
        Save
      </Button>
    </FormContainer>
  );
};

export default SettingsSecurityForm;
