import { IStandardFormProps } from '@sharedTypes/forms';

export type TSettingsSecurityForm = IStandardFormProps & {
  initialValues: {
    password: string;
    securePhone: string;
    backupEmail: string;
  };
};
