export const timezonesOptions = ['Eastern European Time', 'China Taiwan Time', 'Near East Time', 'Greenwich Mean Time'];

export const validators = {
  firstName: {
    required: 'First name is required',
  },
  lastName: {
    required: 'Last name is required',
  },
  timeZone: {
    required: 'Time zone name is required',
  },
  telephone: {
    required: 'Phone number is required',
  },
};
