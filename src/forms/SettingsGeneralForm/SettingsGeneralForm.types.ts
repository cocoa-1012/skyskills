import { IStandardFormProps } from '@sharedTypes/forms';

export type TSettingsGeneralForm = IStandardFormProps & {
  initialValues: {
    firstName: string;
    lastName: string;
    timezone: string;
    telephone: string;
  };
};
