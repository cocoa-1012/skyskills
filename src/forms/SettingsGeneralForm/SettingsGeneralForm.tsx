import React from 'react';
import { FormContainer } from '@forms';
import { Controller, useForm } from 'react-hook-form';
import { Button, DropdownButton, Input, MenuItem, MenuList } from '@components';
import { TSettingsGeneralForm } from './SettingsGeneralForm.types';
import { validators, timezonesOptions } from './SettingsGeneralForm.data';
import styles from './SettingsGeneralForm.module.scss';

const SettingsGeneralForm = ({ isLoading, isSaving, onError, onSubmit, initialValues }: TSettingsGeneralForm) => {
  const {
    control,
    register,
    handleSubmit,
    formState: { errors, isDirty },
  } = useForm();

  return (
    <FormContainer
      {...{
        isLoading,
        isSaving,
      }}
      className={styles.container}
    >
      <Input
        className={styles.filed}
        label="Name"
        defaultValue={initialValues?.firstName}
        placeholder="Add name..."
        error={errors?.firstName?.message}
        {...register('firstName', validators.firstName)}
      />

      <Input
        className={styles.filed}
        label="Surname"
        defaultValue={initialValues?.lastName}
        placeholder="Add surname..."
        error={errors?.lastName?.message}
        {...register('lastName', validators.lastName)}
      />

      <Controller
        name="timezone"
        control={control}
        defaultValue={initialValues.timezone}
        render={({ field }) => (
          <DropdownButton
            className={styles.fieldShort}
            fullWidth
            label="Timezone"
            value={field.value}
            placeholder="Select timezone..."
          >
            <MenuList>
              {timezonesOptions.map((timezone: string) => (
                <MenuItem onClick={() => field.onChange(timezone)}>{timezone}</MenuItem>
              ))}
            </MenuList>
          </DropdownButton>
        )}
      />

      <Input
        className={styles.fieldShort}
        label="Phone Number"
        defaultValue={initialValues?.telephone}
        placeholder="Add phone number..."
        error={errors?.telephone?.message}
        {...register('telephone', validators.telephone)}
      />

      <Button
        className={styles.sendButton}
        variant="contained"
        color="success"
        onClick={() => handleSubmit(onSubmit, onError)()}
        disabled={!isDirty}
      >
        Save
      </Button>
    </FormContainer>
  );
};

export default SettingsGeneralForm;
