export const groupsOptions = ['Drivers', 'Dispatchers', 'Specialist'];

export const validators = {
  name: {
    required: 'Name is required',
  },
  groups: {
    required: 'Groups is required',
  },
  phone: {
    required: 'Phone is required',
  },
  email: {
    required: 'Email is required',
  },
  localTime: {
    required: 'Local time is required',
  },
  currentLocation: {
    required: 'Current location is required',
  },
  deliveryTime: {
    required: 'Delivery time is required',
  },
  homeAddress: {
    required: 'Home address is required',
  },
};
