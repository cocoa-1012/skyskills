import { TTagItem } from '@components/Tag/Tag.types';
import { IStandardFormProps } from '@sharedTypes/forms';

export type TChannelDetailsForm = IStandardFormProps & {
  initialValues: {
    name: string;
    groups: Array<string>;
    labels: Array<TTagItem>;
    phone: string;
    email: string;
    localTime: string;
    currentLocation: string;
    deliveryTime: string;
    homeAddress: string;
  };
};
