import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import { TTagItem } from '@components/Tag/Tag.types';
import { Input, Tags, TextArea, Button, DropdownButton, MenuList, MenuItem, ActionButton, Flex } from '@components';
import {
  LabelsIcon,
  CallIcon,
  EmailIcon,
  LocalTimeIcon,
  CurrentLocationIcon,
  DeliveryTimeIcon,
  HomeAddressIcon,
  GroupsIcon,
  CloseIcon,
} from '@icons';
import FormContainer from '../FormContainer';
import { TChannelDetailsForm } from './ChatDetailsForm.types';
import { validators, groupsOptions } from './ChatDetailsForm.data';
import styles from './ChatDetailsForm.module.scss';

const ChatDetailsForm = ({ isLoading, isSaving, onCancel, initialValues, onError, onSubmit }: TChannelDetailsForm) => {
  const {
    register,
    handleSubmit,
    control,
    formState: { errors, isDirty },
  } = useForm();

  return (
    <FormContainer
      className={styles.container}
      {...{
        isSaving,
        isLoading,
      }}
    >
      <Flex className={styles.header}>
        <Input
          fullWidth
          inputSize="small"
          labelIcon={<EmailIcon />}
          placeholder="Add name..."
          defaultValue={initialValues?.name}
          error={errors?.name?.message}
          {...register('name', validators.name)}
        />
        <ActionButton size="small" icon={<CloseIcon />} onClick={onCancel} />
      </Flex>

      <Flex column className={styles.content}>
        <Controller
          name="groups"
          control={control}
          defaultValue={initialValues.groups}
          render={({ field }) => (
            <DropdownButton
              className={styles.filed}
              fullWidth
              size="small"
              label="Groups"
              value={field.value}
              placeholder="Select group..."
              labelIcon={<GroupsIcon />}
            >
              <MenuList>
                {groupsOptions.map(group => (
                  <MenuItem onClick={() => field.onChange(group)}>{group}</MenuItem>
                ))}
              </MenuList>
            </DropdownButton>
          )}
        />

        <Controller
          name="labels"
          control={control}
          defaultValue={initialValues.labels}
          render={({ field }) => (
            <Tags
              labelIcon={<LabelsIcon />}
              label="Labels"
              size="small"
              placeholder="Add labels..."
              className={styles.filed}
              tags={field.value}
              onAddTag={data => field.onChange([...field.value, data])}
              onRemoveTag={removed =>
                field.onChange(field.value.filter((tag: TTagItem) => tag.value !== removed.value))
              }
            />
          )}
        />

        <Input
          className={styles.filed}
          label="Phone"
          placeholder="Add phone..."
          labelIcon={<CallIcon />}
          defaultValue={initialValues?.phone}
          inputSize="small"
          error={errors?.phone?.message}
          {...register('phone', validators.phone)}
        />

        <Input
          className={styles.filed}
          inputSize="small"
          labelIcon={<EmailIcon />}
          label="Email"
          placeholder="Add email..."
          defaultValue={initialValues?.email}
          error={errors?.email?.message}
          {...register('email', validators.email)}
        />

        <Input
          className={styles.filed}
          inputSize="small"
          label="Local time"
          placeholder="Add local time..."
          labelIcon={<LocalTimeIcon />}
          defaultValue={initialValues?.localTime}
          error={errors?.localTime?.message}
          {...register('localTime', validators.localTime)}
        />

        <Input
          className={styles.filed}
          inputSize="small"
          label="Current location"
          placeholder="Add current location..."
          labelIcon={<CurrentLocationIcon />}
          defaultValue={initialValues?.currentLocation}
          error={errors?.currentLocation?.message}
          {...register('currentLocation', validators.currentLocation)}
        />

        <Input
          className={styles.filed}
          inputSize="small"
          label="Delivery time"
          placeholder="Add delivery time..."
          labelIcon={<DeliveryTimeIcon />}
          defaultValue={initialValues?.deliveryTime}
          error={errors?.deliveryTime?.message}
          {...register('deliveryTime', validators.deliveryTime)}
        />

        <TextArea
          className={styles.filed}
          inputSize="small"
          label="Home address"
          placeholder="Add home address..."
          labelIcon={<HomeAddressIcon />}
          defaultValue={initialValues?.homeAddress}
          error={errors?.homeAddress?.message}
          {...register('homeAddress', validators.homeAddress)}
        />
      </Flex>

      <div className={styles.controls}>
        <Button onClick={onCancel}>Cancel</Button>
        <Button
          type="button"
          variant="contained"
          color="success"
          disabled={!isDirty}
          onClick={() => handleSubmit(onSubmit, onError)()}
        >
          Save
        </Button>
      </div>
    </FormContainer>
  );
};

export default ChatDetailsForm;
