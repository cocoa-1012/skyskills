import clsx from 'clsx';
import React, { Children, KeyboardEvent } from 'react';
import { TFormContainer } from '@forms/FormContainer/FormContainer.types';
import styles from './FormContainer.module.scss';

const FormContainer = ({ children, isLoading, isSaving, className, ...rest }: TFormContainer) => {
  return (
    <form
      {...rest}
      className={clsx(styles.container, className)}
      onSubmit={(event: KeyboardEvent<HTMLFormElement>) => event.preventDefault()}
    >
      {isLoading && <div className={styles.loadingContainer}>Loading ...</div>}

      {isSaving && <div className={styles.savingContainer}>Saving ...</div>}

      {Children.map(children, child => child)}
    </form>
  );
};

export default FormContainer;
