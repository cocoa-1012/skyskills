import { HTMLAttributes } from 'react';

export type TFormContainer = HTMLAttributes<HTMLFormElement> & {
  children: JSX.Element | JSX.Element[];
  isLoading?: boolean;
  isSaving?: boolean;
};
