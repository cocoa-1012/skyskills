import { IStandardFormProps } from '@sharedTypes/forms';
import { TMessageShort, TTextTemplates } from '@components/Conversation/Chat/Chat.types';

export type TSettingsGeneralForm = IStandardFormProps & {
  textTemplates: TTextTemplates;
  initialValues: TMessageShort;
};
