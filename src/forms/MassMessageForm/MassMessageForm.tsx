import React, { ChangeEvent } from 'react';
import { FormContainer } from '@forms';
import { Link } from 'react-router-dom';
import { Controller, useForm } from 'react-hook-form';
import { TTagItem } from '@components/Tag/Tag.types';

import { Button, Checkbox, Counter, Flex, Info, MessageArea, Tags } from '@components';

import { TSettingsGeneralForm } from './MassMessageForm.types';
import styles from './MassMessageForm.module.scss';

const MassMessageForm = ({
  initialValues,
  onSubmit,
  onError,
  isLoading,
  isSaving,
  onCancel,
  textTemplates,
}: TSettingsGeneralForm) => {
  const { watch, control, setValue, handleSubmit } = useForm();

  const isFollowTimeOpen = watch('followTime');

  return (
    <FormContainer
      {...{
        isLoading,
        isSaving,
      }}
      className={styles.container}
    >
      <div>
        <Controller
          name="labels"
          control={control}
          defaultValue={initialValues.labels}
          render={({ field }) => (
            <Tags
              label={
                <Flex className={styles.label}>
                  <span>Send to:</span>
                  <Info>You can mass message users, groups and labels</Info>
                </Flex>
              }
              size="small"
              placeholder="Add users/groups/labels"
              className={styles.filed}
              tags={field.value}
              onAddTag={data => field.onChange([...(field.value || []), data])}
              onRemoveTag={removed =>
                field.onChange(field.value.filter((tag: TTagItem) => tag.value !== removed.value))
              }
            />
          )}
        />

        <Controller
          name="text"
          control={control}
          defaultValue={initialValues.text}
          render={({ field }) => (
            <MessageArea
              label={<p className={styles.label}>Message:</p>}
              placeholder="Send a message to Joe (Ctrl/Cmd + Enter to Send)"
              className={styles.messageArea}
              textTemplates={textTemplates}
              onAttachmentsChange={files => setValue('attachments', files)}
              onTextChange={value => field.onChange(value)}
              info={
                <p>
                  You can mass message users, groups and labels. <Link to="/learn-more">Learn more</Link>
                </p>
              }
            />
          )}
        />

        <Flex column className={styles.advanced}>
          <p className={styles.label}>Advanced Options</p>

          <Controller
            name="follow"
            control={control}
            defaultValue={initialValues.isFollowing}
            render={({ field }) => (
              <Checkbox
                className={styles.advancedOption}
                label="Add me as follower to all conversations"
                onChange={({ target }: ChangeEvent<HTMLInputElement>) => {
                  field.onChange(target.checked);
                }}
              />
            )}
          />

          <Controller
            name="followTime"
            control={control}
            defaultValue={initialValues.isFollowing}
            render={({ field }) => (
              <Checkbox
                className={styles.advancedOption}
                label="Add me for a limited time"
                onChange={({ target }: ChangeEvent<HTMLInputElement>) => {
                  field.onChange(target.checked);
                }}
              />
            )}
          />

          {isFollowTimeOpen && (
            <Flex className={styles.advancedOption}>
              <Counter />
            </Flex>
          )}
        </Flex>
      </div>

      <Flex className={styles.actionControls} horizontalRight>
        <Button size="small" onClick={onCancel}>
          Cancel
        </Button>

        <Button
          onClick={() => handleSubmit(onSubmit, onError)()}
          size="small"
          color="success"
          dropdown={<div>Test</div>}
          variant="contained"
        >
          Send mass message
        </Button>
      </Flex>
    </FormContainer>
  );
};

export default MassMessageForm;
