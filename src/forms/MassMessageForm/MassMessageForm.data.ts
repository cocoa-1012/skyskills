export const timezonesOptions = ['Eastern European Time', 'China Taiwan Time', 'Near East Time', 'Greenwich Mean Time'];

export const validators = {
  message: {
    required: 'Message is required',
  },
};
