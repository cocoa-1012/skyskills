export const getInitials = (username: string): string => {
  try {
    const usernameArr = username.split(' ');

    return `${usernameArr[0]}${usernameArr[1]}`.toUpperCase();
  } catch (error) {
    return username;
  }
};
