import dayjs, { Dayjs } from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat';

dayjs.extend(localizedFormat);

export type TDate = Date | Dayjs | string;

export const dt = (date: TDate, format = `hh:mm A`) => dayjs(date).format(format);
