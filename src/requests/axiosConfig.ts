import axios from "axios";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {'Authorization': 'Secret token'}
});

export default axiosInstance;
