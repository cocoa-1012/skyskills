import { TChannel } from "@sharedTypes/channel";
import {TUser} from "@sharedTypes/user";
import axios from "./axiosConfig";

export const fetchUser = async (userUUID: string): Promise<TUser> => {
  try {
    const { data } = await axios.get(`/users/${userUUID}/channels`);
    return data;
  } catch (error) {
    throw new Error('Failed to fetch the user data');
  }
};

export const fetchChannels = async (userUUID: string, groups?: Array<string>): Promise<TChannel[]> => {
  try {
    const { data } = await axios.get(`/user/${userUUID}/channels`, {
      params: {
        groups: groups?.join(',')
      }
    });

    return data;
  } catch (error) {
    throw new Error('Failed to fetch the channels');
  }
};

export const fetchMessages = async (userUUID: string, channelUUID: string) => {
  try {
    const { data } =  await axios.get(`/user/${userUUID}/channels/${channelUUID}/messages`);
    return data;
  } catch(error) {
    throw new Error('Failed to fetch the messages');
  }
};
