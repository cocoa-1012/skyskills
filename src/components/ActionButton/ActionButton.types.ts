import { ButtonHTMLAttributes } from 'react';

export type TActionButton = ButtonHTMLAttributes<HTMLButtonElement> & {
  children?: JSX.Element | string;
  icon?: JSX.Element;
  variant?: 'contained' | 'outlined';
  size?: 'default' | 'small';
  position?: 'left' | 'right';
};
