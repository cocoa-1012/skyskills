import React from 'react';
import clsx from 'clsx';
import { TActionButton } from './ActionButton.types';
import styles from './ActionButton.module.scss';

const ActionButton = ({
  icon,
  children,
  size = 'default',
  position = 'left',
  variant = 'outlined',
  className,
  ...rest
}: TActionButton) => (
  <button
    {...rest}
    className={clsx(
      styles.buttonAction,
      {
        [styles.buttonSmall]: size === 'small',
        [styles.buttonDefault]: size === 'default',
        [styles.buttonLeftIcon]: position === 'left',
        [styles.buttonRightIcon]: position === 'right',
        [styles.buttonColorText]: variant === 'contained',
        [styles.buttonWithoutText]: icon && !children,
      },
      className
    )}
  >
    {icon}
    <div className={styles.buttonTitle}>{children}</div>
  </button>
);

export default ActionButton;
