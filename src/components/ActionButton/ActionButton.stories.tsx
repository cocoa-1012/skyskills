import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { FilterIcon } from '@icons';
import ActionButtonComponent from './ActionButton';

export default {
  "component": ActionButtonComponent,
  "title": `Components/ActionButton`
} as Meta;

const Template: Story<ComponentProps<typeof ActionButtonComponent>> = (args) => <ActionButtonComponent {...args} />;


export const ActionButton = Template.bind({});

ActionButton.args = {
  children: 'ActionButton',
}

export const ActionButtonIcon = Template.bind({});

ActionButtonIcon.args = {
  children: 'ActionButton Icon',
  icon: <FilterIcon/>,
}
