import React, { MouseEvent } from 'react';
import EmojiPicker, { IEmojiData } from 'emoji-picker-react';
import { Dropdown, Tag, UploadFileSimple } from '@components';
import { TTextTemplate } from '@components/Conversation/Chat/Chat.types';
import { CodeIcon, EmojiIcon, ImageIcon, PencilIcon } from '@icons';
import { TMessageElement } from './Controls.types';
import styles from './Controls.module.scss';

const Controls = ({ templates, onEmojiSelect, onTemplateSelect, onFileSelect, ...rest }: TMessageElement) => {
  return (
    <div {...rest} className={styles.container}>
      <UploadFileSimple onFileUpload={onFileSelect}>
        <button className={styles.controlsButton}>
          <ImageIcon />
        </button>
      </UploadFileSimple>

      <Dropdown
        content={
          <div className={styles.emojiPickerContainer}>
            <EmojiPicker
              disableSearchBar
              onEmojiClick={(event: MouseEvent, { emoji }: IEmojiData) => onEmojiSelect(emoji)}
            />
          </div>
        }
      >
        <button className={styles.controlsButton}>
          <EmojiIcon />
        </button>
      </Dropdown>

      <Dropdown
        content={
          <div className={styles.detailsContainer}>
            {templates?.length ? (
              templates.map(({ category, templates: templatesList }) => (
                <div key={category}>
                  <h5 className={styles.detailsTitle}>{category}</h5>
                  {templatesList?.map(({ label }: TTextTemplate) => (
                    <Tag
                      key={label}
                      className={styles.detailsTag}
                      value={label}
                      label={label}
                      onClick={() => onTemplateSelect(label)}
                    />
                  ))}
                </div>
              ))
            ) : (
              <h5>No details found</h5>
            )}
          </div>
        }
      >
        <button className={styles.controlsButton}>
          <CodeIcon />
        </button>
      </Dropdown>

      <button className={styles.controlsButton}>
        <PencilIcon />
      </button>
    </div>
  );
};

export default Controls;
