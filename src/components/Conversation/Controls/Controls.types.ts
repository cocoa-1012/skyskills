import { HTMLAttributes } from 'react';
import { TTextTemplates } from '@components/Conversation/Chat/Chat.types';
import { TSimpleFile } from '@components/UploadFile/UploadFile.types';

export type TMessageElement = HTMLAttributes<HTMLDivElement> & {
  templates: TTextTemplates;
  onFileSelect: (file: TSimpleFile) => void;
  onEmojiSelect: (emoji: string) => void;
  onTemplateSelect: (template: string) => void;
};
