import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import ChatComponent from './Chat';

export default {
  component: ChatComponent,
  title: `Components/Conversation/Chat`,
} as Meta;

const Template: Story<ComponentProps<typeof ChatComponent>> = args => {
  return <ChatComponent {...args} />;
};

export const Chat = Template.bind({});

Chat.args = {};
