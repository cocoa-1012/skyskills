import { useState } from 'react';
import dayjs from 'dayjs';
import { TSimpleFile } from '@components/UploadFile/UploadFile.types';
import { TTextTemplate, TTextTemplateItem } from '@components/Conversation/Chat/Chat.types';
import { TUseChat } from './Chat.types';

const useChat = ({ textTemplates, onMessageSend }: TUseChat) => {
  const [text, setText] = useState<string>('');
  const [files, setFiles] = useState<TSimpleFile[]>([]);

  const transformTextTemplates = (textValue: string) => {
    const unwrappedTemplates = textTemplates?.reduce(
      (result: TTextTemplate[], { templates }: TTextTemplateItem) => result.concat(templates),
      []
    );

    return unwrappedTemplates?.reduce(
      (result: string, { label, value }): string => result.replace(label, value),
      textValue
    );
  };

  const setScheduleTime = (time: number) => {
    if (time) {
      return dayjs()
        .add(1, 'days')
        .set('hours', time)
        .set('minutes', 0)
        .set('seconds', 0)
        .set('milliseconds', 0)
        .format();
    }

    return null;
  };

  const handleSubmitMessage = (time: number) => {
    const attachments = files.map(({ file }) => file);
    const scheduledTime = setScheduleTime(time);

    onMessageSend?.({
      text: transformTextTemplates(text),
      attachments,
      scheduledTime,
    });

    setText('');
    setFiles([]);
  };

  return {
    text,
    setText,
    files,
    setFiles,
    handleSubmitMessage,
  };
};

export default useChat;
