import { HTMLAttributes } from 'react';
import { TMessage } from '@sharedTypes/message';

export type TTextTemplate = {
  value: string;
  label: string;
};

export type TTextTemplateItem = {
  category: string;
  templates: Array<TTextTemplate>;
};

export type TTextTemplates = Array<TTextTemplateItem>;

export type TMessageShort = {
  text: string;
  scheduledTime: null | string;
  attachments: Array<File>;
};

export type TChat = HTMLAttributes<HTMLDivElement> & {
  userUUID: string;
  messages: Array<TMessage> | undefined;
  scheduledMessagesCount: number;
  lastScheduledMessage: TMessage;
  textTemplates: TTextTemplates;
  onMessageSend: (message: TMessageShort) => void;
};

export type TUseChat = {
  textTemplates: TTextTemplates;
  onMessageSend: (message: TMessageShort) => void;
};
