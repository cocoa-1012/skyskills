import React from 'react';
import {Button, MenuItem, MenuList, Message, MessageArea, Scheduled} from '@components';
import { SendEmailIcon } from '@icons';
import { dt } from '@utils';
import { TChat } from './Chat.types';
import useChat from './useChat';
import styles from './Chat.module.scss';

const Chat = ({
  userUUID,
  messages,
  onMessageSend,
  textTemplates,
  lastScheduledMessage,
  scheduledMessagesCount,
  ...rest
}: TChat) => {
  const { handleSubmitMessage } = useChat({ textTemplates, onMessageSend });

  return (
    <div className={styles.container} {...rest}>
      <div className={styles.messagesContainer}>
        {messages?.length ? (
          messages.map(message => (
            <Message
              key={message.uuid}
              isOwner={userUUID === message.from.uuid}
              {...message}
              style={{
                marginBottom: 25,
              }}
            />
          ))
        ) : (
          <h3 className={styles.noMessagesTitle}>No messages found</h3>
        )}

        <Scheduled
          text={lastScheduledMessage.text}
          dateTime={dt(lastScheduledMessage.creationDateTime, 'LLL')}
          creator={lastScheduledMessage.from.uuid === userUUID ? 'You' : lastScheduledMessage.from.username}
          messageCount={scheduledMessagesCount}
          onEdit={() => {
            // TODO: Will be replaced with real action.
            window.alert('Editing scheduled message');
          }}
          onOpen={() => {
            // TODO: Will be replaced with real action.
            window.alert('Open other scheduled message');
          }}
        />
      </div>

      <div className={styles.messageAreaContainer}>
        <MessageArea
          textTemplates={textTemplates}
          onTextChange={() => {}}
          sendButton={
            <Button
              color="success"
              variant="contained"
              dropdown={
                <div className={styles.sendOptionsDropdown}>
                  <MenuList>
                    <MenuItem size="small" color="text">
                      Schedule this messages
                    </MenuItem>
                    <MenuItem withBorder={false} onClick={() => handleSubmitMessage(18)}>
                      Later today (6:00 PM)
                    </MenuItem>
                    <MenuItem onClick={() => handleSubmitMessage(9)}>Tomorrow morning (9:00 AM)</MenuItem>
                    <MenuItem>Custom time</MenuItem>
                  </MenuList>
                </div>
              }
              size="small"
              icon={<SendEmailIcon />}
              onClick={() => handleSubmitMessage(0)}
            >
              Send
            </Button>
          }
        />
      </div>
    </div>
  );
};

export default Chat;
