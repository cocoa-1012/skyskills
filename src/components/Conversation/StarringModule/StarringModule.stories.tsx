import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import StarringModuleComponent from './StarringModule';
import styles from './StarringModule.module.scss';

export default {
  component: StarringModuleComponent,
  title: `Components/StarringModule`,
} as Meta;

const Template: Story<ComponentProps<typeof StarringModuleComponent>> = arg => {
  return (
    <div className={styles.starringModuleStories}>
      <StarringModuleComponent {...arg} />
    </div>
  );
};

export const StarringModule = Template.bind({});

StarringModule.args = {
  defaultOption: 'My Drivers',
  onFavorite: () => {},
  messagesList: [
    {
      id: 1,
      message: 'was able to reach the broker, they will call you for your assigned door.',
      time: 'Mon Dec 06 2021 17:09:32 GMT+0200 (Восточная Европа, стандартное время)',
      favorite: true,
      sender: 'Tom',
    },
    {
      id: 2,
      message: 'Just got a call with a door',
      time: 'Mon Dec 06 2021 17:09:32 GMT+0200 (Восточная Европа, стандартное время)',
      sender: 'Joe Kohorst',
      favorite: true,
    },
    {
      id: 3,
      message: 'was able to reach the broker, they will call you for your assigned door.',
      time: 'Mon Dec 06 2021 17:09:32 GMT+0200 (Восточная Европа, стандартное время)',
      favorite: true,
      sender: 'John',
    },
  ],
  driversList: [
    {
      id: 1,
      fullName: 'Tom',
    },
    {
      id: 2,
      fullName: 'Joe Kohorst',
    },
    {
      id: 3,
      fullName: 'John',
    },
  ],
};
