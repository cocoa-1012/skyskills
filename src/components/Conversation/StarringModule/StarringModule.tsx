import React, { ChangeEvent, useState } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { DropdownButton, Input, MenuItem, MenuList, StarringMessage } from '@components';
import { SearchIcon } from '@icons';
import styles from './StarringModule.module.scss';
import { TDriver, TMessage, TStarringModule } from './StarringModule.types';

const StarringModule = ({ defaultOption, onFavorite, messagesList, driversList }: TStarringModule) => {
  const [searchQuery, setSearchQuery] = useState<string>('');
  const [selectedDriver, setSelectedDriver] = useState<string>('');
  return (
    <div className={styles.starringModule}>
      <div className={styles.starringModuleHeader}>
        <Input
          placeholder="Search starred items..."
          iconLeft={<SearchIcon />}
          className={styles.starringModuleInput}
          inputSize="small"
          onChange={(e: ChangeEvent<HTMLInputElement>) => setSearchQuery(e.target.value)}
        />
        <DropdownButton
          size="small"
          className={styles.starringModuleButton}
          value={selectedDriver}
          placeholder={defaultOption}
        >
          <MenuList>
            {[
              <MenuItem color="primary" onClick={() => setSelectedDriver('')} size="small">
                {defaultOption}
              </MenuItem>,
              ...driversList.map((driver: TDriver) => (
                <MenuItem
                  key={driver.id}
                  color="primary"
                  onClick={() => setSelectedDriver(driver.fullName)}
                  size="small"
                >
                  {driver.fullName}
                </MenuItem>
              )),
            ]}
          </MenuList>
        </DropdownButton>
      </div>
      <Scrollbars className={styles.starringModuleScrollbar}>
        <div className={styles.starringModuleScrollbarWrapper}>
          {messagesList
            .filter(
              (message: TMessage) =>
                (!selectedDriver || selectedDriver === message.sender) &&
                (!searchQuery ||
                  (message.favorite &&
                    (message.sender.toLowerCase().indexOf(searchQuery.toLowerCase()) > -1 ||
                      message.message.toLowerCase().indexOf(searchQuery.toLowerCase()) > -1)))
            )
            .map((message: TMessage) => (
              <StarringMessage
                key={message.id}
                message={message.message}
                time={message.time}
                favorite={message.favorite}
                sender={message.sender}
                onFavorite={() => onFavorite(message)}
              />
            ))}
        </div>
      </Scrollbars>
    </div>
  );
};

export default StarringModule;
