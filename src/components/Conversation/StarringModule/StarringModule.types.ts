import { HTMLAttributes } from 'react';

export type TDriver = {
  id: string | number;
  fullName: string;
};

export type TStarringMessage = HTMLAttributes<HTMLDivElement> & {
  message: string;
  favorite?: boolean;
  sender: string;
  time: string;
  onFavorite: Function;
  size?: 'small' | 'default';
};

export type TMessage = {
  id: number;
  message: string;
  favorite?: boolean;
  sender: string;
  time: string;
};

export type TStarringModule = HTMLAttributes<HTMLDivElement> & {
  defaultOption: string;
  onFavorite: (message: TMessage) => void;
  messagesList: TMessage[];
  driversList: TDriver[];
};
