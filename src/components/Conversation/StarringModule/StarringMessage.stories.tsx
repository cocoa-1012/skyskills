import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import MessageComponent from './StarringMessage';

export default {
  component: MessageComponent,
  title: `Components/StarringModule/StarringMessage`,
} as Meta;

const Template: Story<ComponentProps<typeof MessageComponent>> = arg => {
  return <MessageComponent {...arg} />;
};

export const MessageDefault = Template.bind({});

MessageDefault.args = {
  favorite: false,
  message: 'Simple text here',
  time: 'Mon Dec 06 2021 17:09:32 GMT+0200 (Восточная Европа, стандартное время)',
};

export const MessageSmall = Template.bind({});

MessageSmall.args = {
  favorite: false,
  message: 'Simple text here',
  time: 'Mon Dec 06 2021 17:09:32 GMT+0200 (Восточная Европа, стандартное время)',
  size: 'small',
};
