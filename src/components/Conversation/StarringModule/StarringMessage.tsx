import React, { useState } from 'react';
import dayjs from 'dayjs';
import clsx from 'clsx';
import { ActionButton } from '@components';
import { RightIcon, StarActiveIcon, StarDefaultIcon } from '@icons';

import styles from './StarringModule.module.scss';
import { TStarringMessage } from './StarringModule.types';

const StarringMessage = ({
  message,
  time,
  sender,
  favorite = false,
  size = 'default',
  onFavorite,
}: TStarringMessage) => {
  const [isFavorite, setIsFavorite] = useState<boolean>(favorite);

  return (
    <div
      className={clsx(styles.starringModuleItem, {
        [styles.smallItem]: size === 'small',
        [styles.defaultItem]: size === 'default',
      })}
    >
      <ActionButton
        className={clsx(styles.starringModuleItemWrapper, { [styles.withoutFavorite]: !isFavorite })}
        icon={isFavorite ? <StarActiveIcon /> : <StarDefaultIcon />}
        onClick={() => {
          setIsFavorite(!isFavorite);
          onFavorite();
        }}
      >
        {message}
      </ActionButton>
      <div className={styles.starringModuleItemSender}>
        <p>
          by {sender} {dayjs(time).format('h:mm A')}
        </p>
        <div className={styles.starringModuleItemSenderIcon}>
          <RightIcon />
        </div>
      </div>
    </div>
  );
};

export default StarringMessage;
