import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import MessageAreaComponent from './MessageArea';

export default {
  component: MessageAreaComponent,
  title: `Components/Conversation/MessageArea`,
} as Meta;

const Template: Story<ComponentProps<typeof MessageAreaComponent>> = args => {
  return <MessageAreaComponent {...args} />;
};

export const MessageArea = Template.bind({});

MessageArea.args = {
  placeholder: 'Write a message...',
};
