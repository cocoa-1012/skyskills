import React, { ChangeEvent, useState } from 'react';
import clsx from 'clsx';
import { FilePreview, Controls, Info, ImageCarousel } from '@components';
import { TSimpleFile } from '@components/UploadFile/UploadFile.types';
import { TMessageArea } from './MessageArea.types';
import styles from './MessageArea.module.scss';
import useMessagesArea from './useMessageArea';

const MessageArea = ({
  label,
  info,
  sendButton,
  textTemplates,
  onTextChange,
  onAttachmentsChange,
  className,
  ...rest
}: TMessageArea) => {
  const [showImageCarousel, setShowImageCarousel] = useState<Boolean>(false);
  const [selectedImage, setSelectedImage] = useState<any>(0);
  const { text, files, removeFile, onFilesChange, onMessageChange } = useMessagesArea({
    onTextChange,
    onAttachmentsChange,
  });

  const openImageCarousel = (index: number) => {
    setShowImageCarousel(true);
    setSelectedImage(index);
  };

  return (
    <div className={clsx(styles.container, className)}>
      <div className={styles.label}>{label}</div>

      <div className={styles.messageAreaContainer}>
        <div className={styles.messageArea}>
          <textarea
            {...rest}
            className={styles.textField}
            value={text}
            onChange={({ target }: ChangeEvent<HTMLTextAreaElement>) => onMessageChange(target.value)}
          />

          <div className={styles.filesContainer}>
            {files?.map((file: TSimpleFile, index) => (
              <FilePreview
                key={file.file.name}
                size="large"
                className={styles.filePreview}
                type={file?.file.type.includes('image') ? 'image' : 'pdf'}
                image={file.link}
                detailed={false}
                onRemove={() => removeFile(file)}
                onClick={() => openImageCarousel(index)}
              />
            ))}
          </div>
          {showImageCarousel && (
            <ImageCarousel
              isOpen
              files={files}
              onClose={() => setShowImageCarousel(false)}
              selectedImage={selectedImage}
            />
          )}

          <div className={styles.downBarContainer}>
            <Controls
              templates={textTemplates}
              onFileSelect={(file: TSimpleFile) => onFilesChange([...files, file])}
              onEmojiSelect={(emoji: string) => onMessageChange(text + emoji)}
              onTemplateSelect={(template: string) => onMessageChange(`${text} ${template}`)}
            />

            {sendButton}
          </div>
        </div>

        {info && <Info className={styles.info}>{info}</Info>}
      </div>
    </div>
  );
};

export default MessageArea;
