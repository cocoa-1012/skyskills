import { useState } from 'react';
import { TSimpleFile } from '@components/UploadFile/UploadFile.types';
import { TUseMessageArea } from '@components/Conversation/MessageArea/MessageArea.types';

const useMessageArea = ({ onAttachmentsChange, onTextChange }: TUseMessageArea) => {
  const [text, setText] = useState<string>('');
  const [files, setFiles] = useState<TSimpleFile[]>([]);

  const onMessageChange = (value: string) => {
    setText(value);
    onTextChange(value);
  };

  const onFilesChange = (fileList: TSimpleFile[]) => {
    setFiles(fileList);
    onAttachmentsChange?.(fileList);
  };

  const removeFile = (file: TSimpleFile) => {
    const filteredFiles = files.filter((f: TSimpleFile) => f?.file?.name !== file.file.name);

    setFiles(filteredFiles);
    onAttachmentsChange?.(filteredFiles);
  };

  return {
    text,
    setText,
    files,
    setFiles,
    removeFile,
    onFilesChange,
    onMessageChange,
  };
};

export default useMessageArea;
