import { InputHTMLAttributes } from 'react';
import { TTextTemplates } from '@components/Conversation/Chat/Chat.types';
import { TSimpleFile } from '@components/UploadFile/UploadFile.types';

export type TUseMessageArea = {
  onTextChange: (value: string) => void;
  onAttachmentsChange?: (files: TSimpleFile[]) => void;
};

export type TMessageArea = InputHTMLAttributes<HTMLTextAreaElement> &
  TUseMessageArea & {
    sendButton?: JSX.Element;
    textTemplates: TTextTemplates;
    label?: string | JSX.Element;
    info?: string | JSX.Element;
  };
