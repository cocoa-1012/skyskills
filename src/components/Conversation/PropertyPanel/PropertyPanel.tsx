import React, { useState } from 'react';
import clsx from 'clsx';
import {
  CallIcon,
  HandUpIcon,
  SearchIcon,
  CloseIcon,
  ConversionScale,
  NotesPaperIcon,
  ArrowDownNotFilledIcon,
  ArrowUpNotFilledIcon,
} from '@icons';
import { ActionButton, Button, Input } from '@components';
import { TPropertyPanel } from './PropertyPanel.types';
import styles from './PropertyPanel.module.scss';

const PropertyPanel = ({
  phone,
  name,
  follower,
  timeCall,
  infoName,
  infoText,
  variant = 'default',
  onCall,
  onHangup,
  onFollowing,
  onSearch,
}: TPropertyPanel) => {
  const [isSearchOpen, setIsSearchOpen] = useState<boolean>(false);
  return (
    <div className={styles.propertyPanel}>
      <div className={styles.propertyPanelLeft}>
        <h5>{name}</h5>
        <p>{phone}</p>
      </div>
      <div
        className={clsx(styles.propertyPanelRight, {
          [styles.propertyPanelRightSearch]: isSearchOpen,
        })}
      >
        {variant === 'hangUp' && (
          <div className={styles.timeCall}>
            {timeCall}
            <ConversionScale />
          </div>
        )}
        <div className={styles.buttonCallWrapper}>
          <Button
            color={variant === 'view' || variant === 'default' ? 'success' : 'danger'}
            variant="contained"
            icon={variant === 'view' || variant === 'default' ? <CallIcon /> : <HandUpIcon />}
            onClick={variant === 'hangUp' ? onHangup : onCall}
          >
            {variant === 'hangUp' ? 'Hang Up' : 'Call'}
          </Button>
        </div>
        {variant === 'view' && infoText && infoName && (
          <div className={styles.infoWrapper}>
            <NotesPaperIcon />
            <h5>{infoName}:</h5>
            <p>{infoText}</p>
          </div>
        )}
        {!isSearchOpen && (
          <ActionButton position="right" color={variant === 'view' ? 'default' : 'primary'} onClick={onFollowing}>
            Following
          </ActionButton>
        )}
        {follower && (
          <div className={styles.follower}>
            <h5>{follower}</h5>
            {follower > 1 ? 'Followers' : 'Follower'}
          </div>
        )}
        {isSearchOpen && (
          <div className={styles.searchOpened}>
            <p>1 of 5 matches</p>
            <div className={styles.searchField}>
              <Input
                fullWidth
                iconLeft={<SearchIcon />}
                placeholder="Search current conversation..."
                onChange={onSearch}
              />
            </div>

            <ActionButton variant="contained" icon={<ArrowUpNotFilledIcon />} />

            <ActionButton variant="contained" icon={<ArrowDownNotFilledIcon />} />
          </div>
        )}

        <ActionButton
          className={styles.search}
          icon={!isSearchOpen ? <SearchIcon /> : <CloseIcon />}
          onClick={() => setIsSearchOpen(!isSearchOpen)}
        />
      </div>
    </div>
  );
};

export default PropertyPanel;
