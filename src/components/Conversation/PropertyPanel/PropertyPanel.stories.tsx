import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import PropertyPanelComponent from './PropertyPanel';

export default {
  component: PropertyPanelComponent,
  title: `Components/PropertyPanel`,
} as Meta;

const Template: Story<ComponentProps<typeof PropertyPanelComponent>> = args => {
  return <PropertyPanelComponent {...args} />;
};

export const PropertyPanelDefault = Template.bind({});

PropertyPanelDefault.args = {
  name: 'Joe Kohorst',
  phone: '(317) 652-1845',
  follower: 2,
};

export const PropertyPanelHangUp = Template.bind({});

PropertyPanelHangUp.args = {
  variant: 'hangUp',
  name: 'Joe Kohorst',
  phone: '(317) 652-1845',
  follower: 2,
  timeCall: '0:01',
};

export const PropertyPanelView = Template.bind({});

PropertyPanelView.args = {
  variant: 'view',
  name: 'Joe Kohorst',
  phone: '(317) 652-1845',
  follower: 1,
  infoText: 'This is an example of a note/reas...',
  infoName: 'Marc Brown',
};
