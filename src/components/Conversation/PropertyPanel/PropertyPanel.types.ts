import { HTMLAttributes, MouseEvent, ChangeEventHandler } from 'react';

export type TPropertyPanel = HTMLAttributes<HTMLDivElement> & {
  name: string;
  phone: string;
  follower?: number | string;
  timeCall?: number | string;
  variant?: 'default' | 'hangUp' | 'view';
  infoName?: string;
  infoText?: string;
  onCall?: (event: MouseEvent<HTMLButtonElement>) => void;
  onHangup?: (event: MouseEvent<HTMLButtonElement>) => void;
  onFollowing?: (event: MouseEvent<HTMLButtonElement>) => void;
  onSearch?: ChangeEventHandler<HTMLInputElement> | undefined;
};
