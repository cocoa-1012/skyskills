import React from 'react';
import { PencilAlternateIcon, LocalTimeIcon } from '@icons';
import { TScheduled } from './Scheduled.types';
import styles from './Scheduled.module.scss';

const Scheduled = ({ text, dateTime, creator, messageCount, onEdit, onOpen, ...rest }: TScheduled) => {
  return (
    <div {...rest} className={styles.container}>
      <div className={styles.containerContent}>
        <button>
          <LocalTimeIcon />
        </button>

        <div className={styles.textContainer}>
          <p className={styles.dateTime}>
            Scheduled for {dateTime} by {creator}
          </p>
          <p className={styles.text}>{text}</p>
        </div>

        <button onClick={onEdit}>
          <PencilAlternateIcon />
        </button>
      </div>

      {messageCount > 1 && (
        <div className={styles.openMore} onClick={onOpen}>
          + {messageCount} more message{messageCount > 1 && 's'} scheduled
        </div>
      )}
    </div>
  );
};

export default Scheduled;
