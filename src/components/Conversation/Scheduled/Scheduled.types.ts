import { HTMLAttributes } from 'react';

export type TScheduled = HTMLAttributes<HTMLDivElement> & {
  text: string;
  dateTime: string;
  creator: string;
  messageCount: number;
  onEdit: () => void;
  onOpen: () => void;
};
