import { HTMLAttributes } from 'react';
import { TMessage } from '@sharedTypes/message';

export type TMessageStatus = 'sent' | 'pending' | 'error';

export enum EMessageStatusCapitalized {
  sent = `Sent`,
  pending = `Pending`,
  error = `Error`,
}

export type TChatMessage = HTMLAttributes<HTMLDivElement> &
  TMessage & {
    status?: TMessageStatus;
    isOwner: boolean;
    onRetry?: () => void;
  };
