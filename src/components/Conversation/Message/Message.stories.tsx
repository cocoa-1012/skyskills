import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import MessageComponent from './Message';

export default {
  component: MessageComponent,
  title: `Components/Conversation/Message`,
} as Meta;

const Template: Story<ComponentProps<typeof MessageComponent>> = args => {
  return <MessageComponent {...args} />;
};

export const Message = Template.bind({});

Message.args = {
  text: 'Some message...',
  from: {
    username: 'John Doe',
    uuid: 'd0c311dc-543b-11ec-bf63-0242ac130002',
  },
  isOwner: true,
  creationDateTime: new Date().toString(),
};
