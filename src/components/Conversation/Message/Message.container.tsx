import React from 'react';
import clsx from 'clsx';
import { getInitials, dt } from '@utils';
import { Avatar } from '@components';
import styles from './Message.module.scss';
import { TChatMessage } from './Message.types';

const MessageContainer = ({
  children,
  isOwner,
  from,
  text,
  color,
  creationDateTime,
  status,
  onRetry,
  attachments,
  ...rest
}: TChatMessage) => {
  return (
    <div
      {...rest}
      className={clsx(styles.container, {
        [styles.messageOwner]: isOwner,
        [styles.error]: status === 'error',
      })}
    >
      {!isOwner && <Avatar size="small">{getInitials(from.username)}</Avatar>}

      <div className={styles.messageContainer}>
        {children}

        <div className={styles.dateTime}>
          {!isOwner && <span className={styles.transparentText}>{dt(creationDateTime)}</span>}

          {isOwner && status === 'error' && (
            <>
              <span className={styles.transparentText}>You · </span>
              <span className={styles.redText}>Send error, retrying in 1 min. </span>
              <span className={styles.retryLink}>Retry now</span>
            </>
          )}

          {isOwner && status !== 'error' && (
            <span className={styles.transparentText}>You · {dt(creationDateTime)} · Sent</span>
          )}
        </div>
      </div>
    </div>
  );
};

export default MessageContainer;
