import React from 'react';
import { FilePreview } from '@components';
import styles from './Message.module.scss';
import { TChatMessage } from './Message.types';
import MessageContainer from './Message.container';

const Message = ({ text, attachments, ...rest }: TChatMessage) => {
  return (
    <MessageContainer {...rest} text={text} attachments={attachments}>
      <div className={styles.message}>
        {text && <p>{text}</p>}

        {attachments?.length &&
          attachments.map(file => (
            <div key={file.uuid} className={styles.attachmentContainer}>
              <FilePreview
                className={styles.attachmentPreview}
                detailed={false}
                size="auto"
                type="image"
                image={file.previewLink}
              />
            </div>
          ))}
      </div>
    </MessageContainer>
  );
};

export default Message;
