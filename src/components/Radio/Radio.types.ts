import { ChangeEvent, InputHTMLAttributes } from 'react';

export type TRadio = InputHTMLAttributes<HTMLInputElement> & {
  id: string;
  label?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
};
