import React, { ChangeEvent, useState } from 'react';
import clsx from 'clsx';
import { TRadio } from './Radio.types';
import styles from './Radio.module.scss';

const Radio = ({ id, label, onChange, ...rest }: TRadio) => {
  const [isChecked, setIsChecked] = useState(!!rest.checked);

  const onChangeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    setIsChecked(!isChecked);

    onChange?.(event);
  };

  return (
    <div className={styles.radioContainer}>
      <label htmlFor={id}>
        <input
          className={styles.hiddenRadio}
          type="radio"
          id={id}
          checked={isChecked}
          onChange={onChangeHandler}
          {...rest}
        />
        <div
          className={clsx(styles.styledRadio, {
            [styles.checked]: !!rest.checked,
          })}
        />

        {label && <span className={styles.label}>{label}</span>}
      </label>
    </div>
  );
};

export default Radio;
