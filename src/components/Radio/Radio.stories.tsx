import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import RadioComponent from './Radio';

export default {
  component: RadioComponent,
  title: `Components/Inputs/Radio`,
} as Meta;

const Template: Story<ComponentProps<typeof RadioComponent>> = args => {
  return <RadioComponent {...args} />;
};

export const Radio = Template.bind({});

Radio.args = {
  checked: false,
};
