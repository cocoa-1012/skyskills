import React from 'react';
import clsx from 'clsx';
import { AlertIcon } from '@icons';
import { TInputWrapper } from './InputWrapper.types';
import styles from './InputWrapper.module.scss';

const InputWrapper = ({
  id,
  children,
  fullWidth,
  inputSize,
  error,
  success,
  label,
  labelIcon,
  iconLeft,
  iconRight,
  className,
}: TInputWrapper) => {
  return (
    <div className={clsx(styles.container, { [styles.fullWidth]: !!fullWidth }, className)}>
      {label && (
        <label htmlFor={id}>
          {labelIcon}
          {label}
        </label>
      )}

      <div
        className={clsx(styles.fieldContainer, {
          [styles.smallSize]: inputSize === 'small',
          [styles.defaultSize]: inputSize !== 'small',
          [styles.leftSpace]: !!iconLeft,
          [styles.rightSpace]: !!iconRight,
          [styles.errorState]: !!error,
          [styles.successState]: !!(success && !error),
        })}
      >
        {iconLeft && (
          <div className={styles.iconLeft}>
            {!error ? (
              iconLeft
            ) : (
              <span className={styles.errorIcon}>
                <AlertIcon />
              </span>
            )}
          </div>
        )}

        {children}

        {iconRight && (
          <div className={styles.iconRight}>
            {!error ? (
              iconRight
            ) : (
              <span className={styles.errorIcon}>
                <AlertIcon />
              </span>
            )}
          </div>
        )}
      </div>

      {error && <p className={styles.errorMessage}>{error}</p>}
    </div>
  );
};

export default InputWrapper;
