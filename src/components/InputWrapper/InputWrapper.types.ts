import { HTMLAttributes } from 'react';

export type TInputWrapper = HTMLAttributes<HTMLDivElement> & {
  children: JSX.Element | string;
  id?: string;
  error?: string;
  success?: boolean;
  label?: string;
  labelIcon?: JSX.Element;
  fullWidth?: boolean;
  inputSize?: 'small' | 'default';
  iconLeft?: JSX.Element;
  iconRight?: JSX.Element;
};
