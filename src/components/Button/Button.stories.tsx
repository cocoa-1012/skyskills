import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { SendEmailIcon } from '@icons';
import ButtonComponent from './Button';

export default {
  "component": ButtonComponent,
  "title": `Components/Button`
} as Meta;

const Template: Story<ComponentProps<typeof ButtonComponent>> = (args) => {
  return (
    <ButtonComponent
      {...args}
    />
  );
};


export const Button = Template.bind({});

Button.args = {
  children: 'Button'
}

export const ButtonIcon = Template.bind({});

ButtonIcon.args = {
  children: 'Button Icon',
  icon: <SendEmailIcon/>,
}

export const ButtonDropdown = Template.bind({});

ButtonDropdown.args = {
  children: 'Button Dropdown',
  dropdown: <div>Test 1</div>,
}
