import React, { useState, useRef } from 'react';
import clsx from 'clsx';
import { TButton } from '@components/Button/Button.types';
import { ArrowDownIcon } from '@icons';
import { useOutsideClick } from '@hooks';
import styles from './Button.module.scss';

const Button = ({
  icon,
  children,
  dropdown,
  size = 'default',
  variant = 'text',
  color = 'primary',
  fullWidth,
  className,
  ...rest
}: TButton) => {
  const [isDropdownOpened, setIsDropdownOpened] = useState<boolean>(false);
  const dropdownRef = useRef<HTMLDivElement>(null);

  useOutsideClick(dropdownRef, () => {
    if (isDropdownOpened) {
      setIsDropdownOpened(false);
    }
  });

  return (
    <div className={clsx(styles.buttonContainer, { [styles.dropdownOpened]: isDropdownOpened })}>
      <button
        {...rest}
        className={clsx(
          styles.button,
          {
            [styles.fullWidth]: !!fullWidth,
            [styles.buttonSmall]: size === 'small',
            [styles.buttonDefault]: size === 'default',
            [styles.buttonLarge]: size === 'large',
            [styles.buttonText]: variant === 'text',
            [styles.buttonContained]: variant === 'contained',
            [styles.buttonOutlined]: variant === 'outlined',
            [styles.buttonPrimary]: color === 'primary',
            [styles.buttonSuccess]: color === 'success',
            [styles.buttonDanger]: color === 'danger',
            [styles.buttonWithIcon]: icon && !children,
            [styles.buttonIconTitle]: icon && children,
          },
          className
        )}
      >
        {icon}
        <div className={styles.buttonTitle}>{children}</div>
        {dropdown && (
          <div
            className={styles.buttonDropdown}
            onClick={() => {
              if (!isDropdownOpened) {
                setIsDropdownOpened(true);
              }
            }}
          >
            <ArrowDownIcon />
          </div>
        )}
      </button>
      {dropdown && (
        <div ref={dropdownRef} className={styles.dropdownContent}>
          {dropdown}
        </div>
      )}
    </div>
  );
};

export default Button;
