import { ButtonHTMLAttributes } from 'react';

export type TButton = ButtonHTMLAttributes<HTMLButtonElement> & {
  children?: JSX.Element | string;
  dropdown?: JSX.Element;
  icon?: JSX.Element;
  size?: 'default' | 'small' | 'large';
  variant?: 'text' | 'contained' | 'outlined';
  color?: 'primary' | 'success' | 'danger';
  fullWidth?: boolean;
};
