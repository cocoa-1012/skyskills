import React, { useState, useRef, useEffect } from 'react';
import clsx from 'clsx';
import { TModal } from '@components/Modal/Modal.types';
import { Flex, Portal } from '@components';
import { useOutsideClick } from '@hooks';
import styles from './Modal.module.scss';

const Modal = ({ children, isVisible, headTitle, onCancel, cancelButton, successButton, className }: TModal) => {
  const [isModalOpened, setIsModalOpened] = useState<boolean>(false);
  const modalRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    setIsModalOpened(!!isVisible);
  }, [isVisible]);

  useOutsideClick(modalRef, () => {
    if (isModalOpened) {
      setIsModalOpened(false);
      onCancel();
    }
  });

  return isModalOpened ? (
    <Portal>
      <Flex className={styles.backdropmodal}>
        <div ref={modalRef} className={clsx(styles.modalMain, className)}>
          {headTitle}

          <div>{children}</div>

          {(cancelButton || successButton) && (
            <Flex className={styles.controls}>
              {cancelButton}
              {successButton}
            </Flex>
          )}
        </div>
      </Flex>
    </Portal>
  ) : null;
};

export default Modal;
