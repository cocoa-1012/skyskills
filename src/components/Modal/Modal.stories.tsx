import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { Button } from '@components';
import ModalComponent from './Modal';

export default {
    component: ModalComponent,
    title: `Components/Modal`,
} as Meta;

const Template: Story<ComponentProps<typeof ModalComponent>> = args => <ModalComponent {...args} />;

export const Modal = Template.bind({});

Modal.args = {
    isVisible: false,
    title: "Modal",
    cancelButton: <Button color="danger" variant="contained">Cancle</Button>,
    successButton: <Button color="success" variant="contained">Submit</Button>
};