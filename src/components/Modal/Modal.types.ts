import { HTMLAttributes } from 'react';

export type TModal = HTMLAttributes<HTMLDivElement> & {
  isVisible?: boolean;
  headTitle?: JSX.Element;
  cancelButton?: JSX.Element;
  successButton?: JSX.Element;
  onCancel: () => void;
};
