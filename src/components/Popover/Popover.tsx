import React, { useRef, useState } from 'react';
import clsx from 'clsx';
import { useOutsideClick } from '@hooks';
import { TPopover } from '@components/Popover/Popover.types';
import styles from './Popover.module.scss';

const Popover = ({ title, subTitle, description, children }: TPopover) => {
  const [isPopoverOpened, setIsPopoverOpened] = useState<boolean>(false);
  const popoverRef = useRef(null);

  useOutsideClick(popoverRef, () => {
    if (isPopoverOpened) {
      setIsPopoverOpened(false);
    }
  });

  return (
    <div className={clsx(styles.popoverContainer, { [styles.popoverOpened]: isPopoverOpened })}>
      <button
        onClick={() => {
          if (!isPopoverOpened) {
            setIsPopoverOpened(true);
          }
        }}
      >
        {children}
      </button>
      <div ref={popoverRef} className={styles.popover}>
        {title && <div className={styles.popoverTitle}>{title}</div>}
        {subTitle && <div className={styles.popoverSubTitle}>{subTitle}</div>}
        {description}
      </div>
    </div>
  );
};

export default Popover;
