import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { Button } from '@components';
import PopoverComponent from './Popover';

export default {
  "component": PopoverComponent,
  "title": `Components/Popover`
} as Meta;

const Template: Story<ComponentProps<typeof PopoverComponent>> = (args) => <PopoverComponent {...args} />;

export const Popover = Template.bind({});

Popover.args = {
  children: <Button>Popover Button</Button>,
  description: 'Help text that explains something a bit more elaborate',
  title: 'Popover Title',
  subTitle: 'Subtitle',
}
