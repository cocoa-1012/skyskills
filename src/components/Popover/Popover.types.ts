import { ButtonHTMLAttributes } from 'react';

export type TPopover = ButtonHTMLAttributes<HTMLButtonElement> & {
  children: JSX.Element;
  title?: string;
  subTitle?: string;
  description?: string;
}
