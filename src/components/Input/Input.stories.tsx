import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { SearchIcon } from '@icons';
import InputComponent from './Input';

export default {
  component: InputComponent,
  title: `Components/Inputs`,
} as Meta;

const Template: Story<ComponentProps<typeof InputComponent>> = args => {
  return <InputComponent {...args} />;
};

export const Input = Template.bind({});

Input.args = {
  placeholder: 'Fill in this field...',
  success: false,
};

export const InputIcon = Template.bind({});

InputIcon.args = {
  placeholder: 'Fill in this field...',
  iconLeft: <SearchIcon />,
};

export const InputStates = Template.bind({});

InputStates.args = {
  placeholder: 'Fill in this field...',
  iconLeft: <SearchIcon />,
  value: 'Some text',
  error: 'Oops! Something went wrong',
};

export const InputFullWidth = Template.bind({});

InputFullWidth.args = {
  value: 'Some text',
  iconLeft: <SearchIcon />,
  success: true,
  fullWidth: true,
};
