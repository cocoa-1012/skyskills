import React, { forwardRef } from 'react';
import { InputWrapper } from '@components';
import { TInput, TRef } from './Input.types';

const Input = forwardRef(
  (
    { id, fullWidth, inputSize, error, success, label, iconLeft, iconRight, className, labelIcon, ...rest }: TInput,
    ref: TRef
  ) => {
    return (
      <InputWrapper
        {...{
          fullWidth,
          inputSize,
          error,
          success,
          label,
          iconLeft,
          iconRight,
          labelIcon,
        }}
        className={className}
      >
        <input ref={ref} type="text" id={id} {...rest} />
      </InputWrapper>
    );
  }
);

export default Input;
