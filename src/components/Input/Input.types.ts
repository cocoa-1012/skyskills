import { ForwardedRef, InputHTMLAttributes } from 'react';
import { TInputWrapper } from '@components/InputWrapper/InputWrapper.types';

export type TInput = InputHTMLAttributes<HTMLInputElement> & Partial<TInputWrapper>;
export type TRef = ForwardedRef<HTMLInputElement> | null;
