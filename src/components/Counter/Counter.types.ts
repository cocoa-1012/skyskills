import { HTMLAttributes } from 'react';

export type TCount = HTMLAttributes<HTMLDivElement> & {
  min?: number | string;
  max?: number | string;
};
