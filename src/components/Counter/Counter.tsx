import React, { useState } from 'react';
import { TCount } from '@components/Counter/Counter.types';
import { PlusIcon, MinusIcon } from '@icons';
import styles from './Counter.module.scss';

const Counter = ({ min = 0, max }: TCount) => {
  const [count, setCount] = useState<number>(0);

  return (
    <div className={styles.countContainer}>
      <button className={styles.countBlockLeft} onClick={() => min !== count && setCount(count - 1)}>
        <MinusIcon />
      </button>
      <div className={styles.countBlockCenter}>{count}</div>
      <button className={styles.countBlockRight} onClick={() => max !== count && setCount(count + 1)}>
        <PlusIcon />
      </button>
    </div>
  );
};

export default Counter;
