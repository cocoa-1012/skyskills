import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import CounterComponent from './Counter';

export default {
  "component": CounterComponent,
  "title": `Components/ActionButton`
} as Meta;

const Template: Story<ComponentProps<typeof CounterComponent>> = (args) => <CounterComponent {...args} />;


export const Counter = Template.bind({});

Counter.args = {
  min: 0,
  max: 10,
}
