import React, { useState, MouseEvent, useRef } from 'react';
import clsx from 'clsx';
import { ArrowDownIcon } from '@icons';
import { useOutsideClick } from '@hooks';
import { TDropdownButton } from '@components/DropdownButton/DropdownButton.types';
import styles from './DropdownButton.module.scss';

const DropdownButton = ({
  children,
  value,
  placeholder,
  onClick,
  fullWidth,
  size = 'default',
  label,
  labelIcon,
  className,
}: TDropdownButton) => {
  const [isDropdownOpened, setIsDropdownOpened] = useState<boolean>(false);
  const dropdownRef = useRef(null);

  useOutsideClick(dropdownRef, () => {
    if (isDropdownOpened) {
      setIsDropdownOpened(false);
    }
  });

  return (
    <div
      className={clsx(
        styles.buttonContainer,
        {
          [styles.dropdownOpened]: isDropdownOpened,
          [styles.fullWidth]: !!fullWidth,
        },
        className
      )}
    >
      {label && (
        <div className={styles.label}>
          {labelIcon}
          {label}
        </div>
      )}
      <button
        className={clsx(styles.button, {
          [styles.buttonSmall]: size === 'small',
          [styles.buttonDefault]: size === 'default',
        })}
        onClick={(event: MouseEvent<HTMLButtonElement>) => {
          event.preventDefault();
          onClick?.(event);
          setIsDropdownOpened(!isDropdownOpened);
        }}
      >
        {value && <div className={styles.buttonValue}>{value}</div>}

        {!value && <div className={styles.buttonPlaceholder}>{placeholder}</div>}

        <div className={styles.buttonDropdown}>
          <ArrowDownIcon />
        </div>
      </button>

      <div ref={dropdownRef} className={styles.dropdownContent}>
        {children}
      </div>
    </div>
  );
};

export default DropdownButton;
