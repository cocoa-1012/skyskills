import { ButtonHTMLAttributes } from 'react';

export type TDropdownButton = ButtonHTMLAttributes<HTMLButtonElement> & {
  children?: JSX.Element;
  value?: string;
  placeholder?: string;
  label?: string;
  labelIcon?: JSX.Element;
  size?: 'default' | 'small' | 'large';
  fullWidth?: boolean;
};
