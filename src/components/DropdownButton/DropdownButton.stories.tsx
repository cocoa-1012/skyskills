import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { MenuItem, MenuList } from '@components';
import ButtonComponent from './DropdownButton';

export default {
  component: ButtonComponent,
  title: `Components/DropdownButton`,
} as Meta;

const Template: Story<ComponentProps<typeof ButtonComponent>> = args => {
  return <ButtonComponent {...args} />;
};

export const DropdownButton = Template.bind({});

DropdownButton.args = {
  placeholder: 'All Drivers',
  children: (
    <MenuList>
      <MenuItem color="primary">John Koslow</MenuItem>
      <MenuItem color="primary">John Koslow</MenuItem>
      <MenuItem color="primary">John Koslow</MenuItem>
      <MenuItem color="primary">John Koslow</MenuItem>
      <MenuItem color="primary">John Koslow</MenuItem>
      <MenuItem color="primary">John Koslow</MenuItem>
      <MenuItem color="primary">John Koslow</MenuItem>
    </MenuList>
  ),
};
