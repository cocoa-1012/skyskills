import { HTMLAttributes } from 'react';

export type TFlex = HTMLAttributes<HTMLDivElement> & {
  row?: boolean;
  column?: boolean;
  shrink?: boolean;
  grow?: boolean;
  fullHeight?: boolean;
  horizontalLeft?: boolean;
  horizontalRight?: boolean;
  horizontalCenter?: boolean;
  verticalTop?: boolean;
  verticalCenter?: boolean;
  verticalBottom?: boolean;
};
