import React from 'react';
import clsx from 'clsx';
import styles from './Flex.module.scss';
import { TFlex } from './Flex.types';

const Flex = ({
  children,
  column,
  row,
  grow,
  shrink,
  fullHeight,
  horizontalLeft,
  horizontalRight,
  horizontalCenter,
  verticalTop,
  verticalBottom,
  verticalCenter,
  className,
  ...rest
}: TFlex) => {
  return (
    <div
      {...rest}
      className={clsx(styles.container, className, {
        [styles.row]: !!row,
        [styles.column]: !!column,
        [styles.grow]: !!grow,
        [styles.shrink]: !!shrink,
        [styles.fullHeight]: !!fullHeight,
        [styles.horizontalLeft]: !!horizontalLeft,
        [styles.horizontalRight]: !!horizontalRight,
        [styles.horizontalCenter]: !!horizontalCenter,
        [styles.verticalTop]: !!verticalTop,
        [styles.verticalBottom]: !!verticalBottom,
        [styles.verticalCenter]: !!verticalCenter,
      })}
    >
      {children}
    </div>
  );
};

export default Flex;
