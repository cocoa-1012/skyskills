import React from 'react';
import clsx from 'clsx';
import {Flex} from "@components";
import styles from './Header.module.scss';
import { THeader } from './Header.types';

const Header = ({ children, className, ...rest }: THeader) => {
  return (
    <Flex shrink {...rest} className={clsx(styles.container, className)}>
      {children}
    </Flex>
  );
};

export default Header;
