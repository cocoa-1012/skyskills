import React from 'react';
import clsx from 'clsx';
import styles from './Content.module.scss';
import { TContent } from './Content.types';

const Content = ({ children, className, ...rest }: TContent) => {
  return (
    <div {...rest} className={clsx(styles.container, className)}>
      {children}
    </div>
  );
};

export default Content;
