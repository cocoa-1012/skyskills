import { HTMLAttributes } from 'react';

export type TSidebar = HTMLAttributes<HTMLDivElement> & {
  isVisible?: boolean;
};
