import React from 'react';
import clsx from 'clsx';
import styles from './Sidebar.module.scss';
import { TSidebar } from './Sidebar.types';

const Sidebar = ({ children, isVisible = true, className, ...rest }: TSidebar) => {
  return isVisible ? (
    <div {...rest} className={clsx(styles.container, className)}>
      {children}
    </div>
  ): null
};

export default Sidebar;
