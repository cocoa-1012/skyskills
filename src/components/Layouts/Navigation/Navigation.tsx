import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { LogoIcon } from '@icons';
import NavigationItem from './NavigationItem';
import { TNavigation } from './Navigation.types';
import styles from './Navigation.module.scss';

const Navigation = ({ topMenu, bottomMenu }: TNavigation) => {
  const { pathname } = useLocation();

  return (
    <div className={styles.container}>
      <div>
        <Link to="/" className={styles.logo}>
          <LogoIcon />
        </Link>

        {topMenu?.length && (
          <div>
            {topMenu?.length &&
              topMenu.map(({ icon, title, route, badge }) => (
                <NavigationItem
                  key={title}
                  isActive={pathname === route}
                  {...{
                    route,
                    icon,
                    title,
                    badge,
                  }}
                />
              ))}
          </div>
        )}
      </div>

      {bottomMenu?.length && (
        <div>
          {bottomMenu?.length &&
            bottomMenu.map(({ icon, title, route, badge }) => (
              <NavigationItem
                key={title}
                isActive={pathname === route}
                {...{
                  route,
                  icon,
                  title,
                  badge,
                }}
              />
            ))}
        </div>
      )}
    </div>
  );
};

export default Navigation;
