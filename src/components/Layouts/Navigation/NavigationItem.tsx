import React from 'react';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import { TNavigationItem } from './Navigation.types';
import styles from './Navigation.module.scss';

const NavigationItem = ({ title, route, icon, badge, isActive }: TNavigationItem) => {
  return (
    <Link
      to={route}
      className={clsx(styles.navItem, {
        [styles.active]: isActive,
      })}
    >
      <div className={styles.navIcon}>
        {icon}

        {badge && <div className={styles.navBadge}>{badge}</div>}
      </div>
      {title}
    </Link>
  );
};

export default NavigationItem;
