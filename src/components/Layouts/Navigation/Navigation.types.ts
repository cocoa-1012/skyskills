import { HTMLAttributes } from 'react';

export type TNavigationItem = {
  title: string;
  route: string;
  icon: JSX.Element;
  badge?: number;
  isActive?: boolean;
};

export type TNavigation = HTMLAttributes<HTMLDivElement> & {
  topMenu?: Array<TNavigationItem>;
  bottomMenu?: Array<TNavigationItem>;
};
