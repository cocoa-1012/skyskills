import { TTagItem } from '@components/Tag/Tag.types';
import { HTMLAttributes } from 'react';

export type TTags = HTMLAttributes<HTMLDivElement> & {
  tags: Array<TTagItem>;
  icon?: JSX.Element;
  label?: string | JSX.Element;
  labelIcon?: JSX.Element;
  size?: 'small' | 'default';
  placeholder?: string;
  onAddTag: (data: TTagItem) => void;
  onRemoveTag: (data: TTagItem) => void;
};
