import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { SearchIcon } from '@icons';
import TagsComponent from './Tags';

export default {
  component: TagsComponent,
  title: `Components/Tags`,
} as Meta;

const Template: Story<ComponentProps<typeof TagsComponent>> = args => {
  return <TagsComponent {...args} />;
};

export const Tags = Template.bind({});

Tags.args = {
  icon: <SearchIcon />,
  placeholder: 'Add some tags...',
};
