import React, { useState, ChangeEvent, KeyboardEvent } from 'react';
import clsx from 'clsx';
import { Input, Tag } from '@components';
import { TTagItem } from '@components/Tag/Tag.types';
import { TTags } from './Tags.types';
import styles from './Tags.module.scss';

const Tags = ({
  tags,
  icon,
  size,
  placeholder,
  onAddTag,
  onRemoveTag,
  label,
  labelIcon,
  className,
  ...rest
}: TTags) => {
  const [inputValue, setInputValue] = useState<string>('');

  const onAddTagHandler = (event: KeyboardEvent<HTMLElement>) => {
    if (event.key === 'Enter' && inputValue?.trim()?.length) {
      onAddTag({
        label: inputValue,
        value: inputValue,
      });

      setInputValue('');
    }
  };

  const onInputChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
    setInputValue(target.value);
  };

  return (
    <div {...rest} className={clsx(styles.container, className)}>
      {label && (
        <span className={styles.tagsLabel}>
          {labelIcon}
          {label}
        </span>
      )}

      <div
        className={clsx(styles.tags, {
          [styles.withIcon]: !!icon,
          [styles.smallSize]: size === 'small',
          [styles.defaultSize]: size !== 'small',
        })}
      >
        {icon && <span className={styles.tagsIcon}>{icon}</span>}
        <div className={styles.tagsContent}>
          {tags?.length ? (
            <div className={styles.tagsList}>
              {tags?.map((tag: TTagItem) => (
                <Tag
                  className={styles.tag}
                  key={tag.value}
                  label={tag.label}
                  value={tag.value}
                  icon={tag.icon}
                  onRemove={onRemoveTag}
                />
              ))}
            </div>
          ) : null}
          <Input
            fullWidth
            inputSize={size}
            className={styles.tagsInput}
            value={inputValue}
            placeholder={placeholder}
            onKeyUp={onAddTagHandler}
            onChange={onInputChange}
          />
        </div>
      </div>
    </div>
  );
};

export default Tags;
