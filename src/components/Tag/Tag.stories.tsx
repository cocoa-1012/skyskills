import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { SearchIcon } from '@icons';
import TagComponent from './Tag';

export default {
  component: TagComponent,
  title: `Components/Tags`,
} as Meta;

const Template: Story<ComponentProps<typeof TagComponent>> = args => {
  return <TagComponent {...args} />;
};

export const Tag = Template.bind({});

Tag.args = {
  label: 'Joe Kohorst',
  value: 'user id',
};

export const TagIcon = Template.bind({});

TagIcon.args = {
  label: 'Joe Kohorst',
  value: 'user id',
  icon: <SearchIcon />,
};
