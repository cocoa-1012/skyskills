import React from 'react';
import clsx from 'clsx';
import { CloseIcon } from '@icons';
import { TTag } from './Tag.types';
import styles from './Tag.module.scss';

const Tag = ({ icon, value, label, onRemove, onClick, className, ...rest }: TTag) => {
  const onRemoveHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    onRemove?.({
      value,
      label,
    });
  };

  const onClickHandler = () => {
    onClick?.({
      value,
      label,
    });
  };

  return (
    <div {...rest} className={clsx(styles.tag, className)} aria-hidden onClick={onClickHandler}>
      {icon && <span className={styles.tagIcon}>{icon}</span>}

      <div className={styles.tagContent}>{label}</div>

      {onRemove && (
        <button type="button" className={styles.tagRemoveButton} onClick={onRemoveHandler}>
          <CloseIcon />
        </button>
      )}
    </div>
  );
};

export default Tag;
