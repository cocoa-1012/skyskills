import { HTMLAttributes } from 'react';

export type TTagItem = {
  value: string;
  label: string;
  icon?: JSX.Element;
};

export type TTag = HTMLAttributes<HTMLDivElement> &
  TTagItem & {
    onRemove?: (data: TTagItem) => void;
    onClick?: (data: TTagItem) => void;
  };
