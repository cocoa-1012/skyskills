import React from 'react';
import clsx from 'clsx';
import { Scrollbars } from 'react-custom-scrollbars';
import { TMenuList } from '@components/Menu/Menu.types';
import styles from './Menu.module.scss';

const MenuList = ({ children, position = 'dropdown' }: TMenuList) => {
  return (
      position === 'vertical' ? (
        <div className={clsx(styles.menuList, styles.menuWithoutScroll)}>
          {children}
        </div>
      ) : (
        <Scrollbars
          className={styles.menuList}
          autoHeight
        >
          {children}
        </Scrollbars>
      )
  )
};

export default MenuList;
