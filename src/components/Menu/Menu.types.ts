import { ButtonHTMLAttributes, HTMLAttributes } from 'react';

export type TMenuItem = ButtonHTMLAttributes<HTMLButtonElement> & {
  children: JSX.Element | JSX.Element[] | string;
  input?: JSX.Element;
  icon?: JSX.Element;
  size?: 'default' | 'small' | 'large';
  color?: 'primary' | 'default' | 'text';
  withBorder?: boolean;
  isActive?: boolean;
};

export type TMenuList = HTMLAttributes<HTMLDivElement> & {
  children: JSX.Element | JSX.Element[];
  position?: 'dropdown' | 'vertical';
};
