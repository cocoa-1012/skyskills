import React from 'react';
import clsx from 'clsx';
import styles from '@components/Menu/Menu.module.scss';
import { TMenuItem } from '@components/Menu/Menu.types';

const MenuItem = ({
  size = 'default',
  color = 'default',
  withBorder = true,
  icon,
  input,
  children,
  isActive,
  ...rest
}: TMenuItem) => {
  return (
    <button
      {...rest}
      className={clsx(styles.menuItem, {
        [styles.menuSmall]: size === 'small',
        [styles.menuDefault]: size === 'default',
        [styles.menuLarge]: size === 'large',
        [styles.menuColorPrimary]: color === 'primary',
        [styles.menuColorDefault]: color === 'default',
        [styles.menuColorText]: color === 'text',
        [styles.active]: !!isActive,
        [styles.menuShadow]: withBorder,
      })}
    >
      {icon && <span className={styles.menuIcon}>{icon}</span>}
      {input && <span className={styles.menuInput}>{input}</span>}
      {children}
    </button>
  );
};

export default MenuItem;
