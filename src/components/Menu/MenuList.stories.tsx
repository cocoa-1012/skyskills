import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { MenuList, MenuItem } from './index';

export default {
  "component": MenuList,
  "title": `Components/MenuList`
} as Meta;

const Template: Story<ComponentProps<typeof MenuList>> = (args) => {
  return (
    <MenuList
      {...args}
    />
  );
};


export const Menu = Template.bind({});

Menu.args = {
  children: [<MenuItem size='small'>John Koslow</MenuItem>, <MenuItem size='small'>John Koslow</MenuItem>,],
}

