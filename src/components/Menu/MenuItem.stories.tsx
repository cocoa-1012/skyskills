import { Meta, Story } from '@storybook/react';
import React, { ComponentProps } from 'react';
import { MenuItem, MenuList } from '@components';
import { FilterIcon } from '@icons';

export default {
  "component": MenuItem,
  "title": `Components/MenuItem`
} as Meta;

const Template: Story<ComponentProps<typeof MenuItem>> = (args) => {
  return (
    <MenuList>
      <MenuItem
        {...args}
      />
    </MenuList>
  );
};

export const MenuItemSimple = Template.bind({});

MenuItemSimple.args = {
  children: "John Koslow",
}

export const MenuItemIcon = Template.bind({});

MenuItemIcon.args = {
  children: "John Koslow",
  icon: <FilterIcon/>,
}

export const MenuItemInput = Template.bind({});

MenuItemInput.args = {
  children: "John Koslow",
  input: <input type='checkbox' />,
}
