import React, { forwardRef } from 'react';
import clsx from 'clsx';
import { InputWrapper } from '@components';
import { TTextArea, TRef } from './TextArea.types';
import styles from './TextArea.module.scss';

const TextArea = forwardRef(
  (
    { id, fullWidth, inputSize, error, success, label, iconLeft, iconRight, className, labelIcon, ...rest }: TTextArea,
    ref: TRef
  ) => {
    return (
      <InputWrapper
        {...{
          fullWidth,
          inputSize,
          error,
          success,
          label,
          labelIcon,
          iconLeft,
          iconRight,
          className,
        }}
      >
        <textarea ref={ref} id={id} {...rest} className={clsx(styles.textArea, className)} />
      </InputWrapper>
    );
  }
);

export default TextArea;
