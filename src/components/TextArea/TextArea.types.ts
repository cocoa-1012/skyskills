import { ForwardedRef, InputHTMLAttributes } from 'react';
import { TInputWrapper } from '@components/InputWrapper/InputWrapper.types';

export type TTextArea = InputHTMLAttributes<HTMLTextAreaElement> & Partial<TInputWrapper>;
export type TRef = ForwardedRef<HTMLTextAreaElement> | null;
