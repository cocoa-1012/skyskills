import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import TextAreaComponent from './TextArea';

export default {
  component: TextAreaComponent,
  title: `Components/Inputs/Textarea`,
} as Meta;

const Template: Story<ComponentProps<typeof TextAreaComponent>> = args => {
  return (
    <TextAreaComponent
      fullWidth
      style={{
        height: 100,
      }}
      {...args}
    />
  );
};

export const TextArea = Template.bind({});

TextArea.args = {
  placeholder: 'Fill in this field...',
};
