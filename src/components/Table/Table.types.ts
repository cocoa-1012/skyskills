import { ButtonHTMLAttributes, HTMLAttributes } from 'react';

export type TColumn = {
  title: string;
  field: string;
  type: string;
  width?: string | number;
  flex?: string | number;
  className?: string;
};

export type TRow = {
  id: string;
  [key: string]: string | string[];
};

export type TTable = ButtonHTMLAttributes<HTMLTableRowElement> & {
  columns: TColumn[];
  data: TRow[];
  className?: string;
  emptyMessage: string;
  isLoading: boolean;
  onRowSelect?: (row: TRow) => void;
};

export type TCell = HTMLAttributes<HTMLTableCellElement> & {
  column: TColumn;
  row: TRow;
};
export type TTextCell = HTMLAttributes<HTMLTableCellElement> & {
  column: TColumn;
  value: string;
};

export type TTagsCell = HTMLAttributes<HTMLTableCellElement> & {
  column: TColumn;
  value: Array<string>;
};
