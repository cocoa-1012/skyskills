import React from 'react';
import {TTextCell} from '@components/Table/Table.types';

const HeadCell = ({ column, value, ...rest }: TTextCell) => {
  return (
    <th
      {...rest}
      style={{
        width: column.width,
        flex: column.flex
      }}
    >
      {value}
    </th>
  );
};

export default HeadCell;
