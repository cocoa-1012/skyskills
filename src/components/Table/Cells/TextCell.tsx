import React from 'react';
import {TTextCell} from '@components/Table/Table.types';

const TextCell = ({ column, value, ...rest }: TTextCell) => {
  return (
    <td
      {...rest}
        style={{
          width: column.width,
          flex: column.flex
        }}
    >
      {value}
    </td>
  );
};

export default TextCell;
