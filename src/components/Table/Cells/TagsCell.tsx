import React from 'react';
import { TTagsCell } from '@components/Table/Table.types';
import { Tag } from '@components';
import styles from '../Table.module.scss';

const TagsCell = ({ column, value, ...rest }: TTagsCell) => {
  return (
    <td
      {...rest}
      style={{
        width: column.width,
        flex: column.flex
      }}
    >
      {
        value.map((item: string) => (
          <Tag
            key={item}
            value={item}
            label={item}
            className={styles.tag}
          >
            {item}
          </Tag>
        ))
      }
    </td>
  );
};

export default TagsCell;
