import React from 'react';
import {TCell} from "@components/Table/Table.types";
import TagsCell from './TagsCell';
import TextCell from './TextCell';
import HeadCell from './HeadCell';

const Cell = ({ column, row }: TCell) => {
  switch (column.type) {
    case 'head':
      return <HeadCell column={column} value={row[column.field] as string} />;

    case 'tags':
      return <TagsCell column={column} value={row[column.field] as string[]} />;

    default:
      return <TextCell column={column} value={row[column.field] as string} />;
  }
};

export default Cell;
