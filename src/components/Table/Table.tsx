import React from 'react';
import clsx from 'clsx';
import { TTable, TColumn, TRow } from '@components/Table/Table.types';
import { LoaderIcon } from '@icons';
import { Flex } from "@components";
import { Cell, HeadCell } from './Cells';
import styles from './Table.module.scss';

const Table = ({ data, columns, onRowSelect, className, emptyMessage, isLoading }: TTable) => {
  return (
    <div className={clsx(styles.container, className)}>
      <table className={styles.table}>
        <thead>
          <tr>
            {columns?.map((cell: TColumn) => (
              <HeadCell key={cell.field} value={cell.title} column={cell} />
            ))}
          </tr>
        </thead>

        <tbody>
        {
          !isLoading && data?.length ? (
            data.map((row: TRow) => (
              <tr key={`table-row-${row.id}`} onClick={() => onRowSelect?.(row)}>
                {
                  columns.map((cell: TColumn) => (
                    <Cell column={cell} row={row} key={`table-cell-${row.id}-${row[cell.field]}`} />
                  ))
                }
              </tr>
            ))
          ) : null
        }

        {
          !isLoading && !data?.length ? (
            <Flex verticalCenter horizontalCenter className={styles.emptyRow}>{emptyMessage}</Flex>
          ) : null
        }

        {
          isLoading ? (
            <Flex verticalCenter horizontalCenter className={styles.emptyRow}>
              <LoaderIcon />
            </Flex>
          ) : null
        }
        </tbody>
      </table>
    </div>
  );
};

export default Table;
