import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import Table from './Table';
import styles from './Table.module.scss';

export default {
  component: Table,
  title: `Components/Table`,
} as Meta;

const Template: Story<ComponentProps<typeof Table>> = args => {
  return (
    <div className={styles.tableStoryBook}>
      <Table {...args} />
    </div>
  );
};

export const TableDefault = Template.bind({});

TableDefault.args = {
  columns: [
    {
      title: 'Name',
      field: 'name',
      type: 'text',
    },
    {
      title: 'Dispatcher',
      field: 'dispatchers',
      type: 'text',
    },
    {
      title: 'Phone',
      field: 'phone',
      type: 'text',
    },
    {
      title: 'Label',
      field: 'labels',
      type: 'text',
    },
  ],
  data: [
    {
      id: '1',
      name: 'Joe Kohorst',
      dispatchers: 'Tom Ivanov',
      phone: '(032)503212',
      labels: ['Label 1', 'Label 2', 'Label 3'],
    },
    {
      id: '2',
      name: 'Joe Kohorst',
      dispatchers: 'Tom Ivanov',
      phone: '(032)503212',
      labels: ['Label 1', 'Label 2'],
    },
    {
      id: '3',
      name: 'Joe Kohorst',
      dispatchers: 'Tom Ivanov',
      phone: '(032)503212',
      labels: ['Label 1'],
    },
    {
      id: '4',
      name: 'Joe Kohorst',
      dispatchers: 'Tom Ivanov',
      phone: '(032)503212',
      labels: ['Label 1', 'Label 2', 'Label 3'],
    },
    {
      id: '5',
      name: 'Joe Kohorst',
      dispatchers: 'Tom Ivanov',
      phone: '(032)503212',
      labels: ['Label 1', 'Label 2', 'Label 3'],
    },
  ],
  emptyMessage: 'No Record Found',
  isLoading: false,
};
