import React from 'react';
import { LoaderIcon } from '@icons';
import {Flex} from "@components";
import { TLoader } from './Loader.types';
import styles from './Loader.module.scss';

const Loader = ({ isVisible }: TLoader) => {

  return isVisible ? (
    <Flex verticalCenter horizontalCenter className={styles.container}>
      <div className={styles.loader}>
        <LoaderIcon />
      </div>
    </Flex>
  ) : null
};

export default Loader;
