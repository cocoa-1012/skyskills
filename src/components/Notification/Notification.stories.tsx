import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { Avatar, Button } from '@components';
import NotificationComponent from './Notification';

export default {
  component: NotificationComponent,
  title: `Components/Notification`,
} as Meta;

const Template: Story<ComponentProps<typeof NotificationComponent>> = args => {
  return <NotificationComponent {...args} />;
};

export const Notification = Template.bind({});

Notification.args = {
  title: 'Notification',
  content: 'Some content here',
  leftComponent: <Avatar>Test</Avatar>,
  rightComponent: <Button>View</Button>,
};
