import React from 'react';
import clsx from 'clsx';
import { TNotification } from './Notification.types';
import styles from './Notification.module.scss';

const Notification = ({ title, content, leftComponent, rightComponent, ...rest }: TNotification) => {
  return (
    <div {...rest} className={styles.notificationContainer}>
      {leftComponent && <div>{leftComponent}</div>}

      <div
        className={clsx(styles.content, {
          [styles.rightBorder]: !!rightComponent,
        })}
      >
        {title && <h5 className={styles.title}>{title}</h5>}

        {content}
      </div>

      {rightComponent && <div>{rightComponent}</div>}
    </div>
  );
};

export default Notification;
