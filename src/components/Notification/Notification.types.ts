import { HTMLAttributes } from 'react';

export type TNotification = HTMLAttributes<HTMLDivElement> & {
  title?: string;
  content: string;
  leftComponent: JSX.Element;
  rightComponent: JSX.Element;
};
