import { HTMLAttributes } from 'react';

export type TInvitedUser = HTMLAttributes<HTMLDivElement> & {
  title: string;
  subTitle: string;
  follow: boolean;
  onFollow: Function;
  note?: string
}
