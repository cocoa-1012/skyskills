import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import InvitedUserComponent from './InvitedUser';

export default {
  component: InvitedUserComponent,
  title: `Components/InvitedUser`,
} as Meta;

const Template: Story<ComponentProps<typeof InvitedUserComponent>> = args => {
  return <InvitedUserComponent {...args} />;
};

export const InvitedUserDefault = Template.bind({});

InvitedUserDefault.args = {
  title: 'John Doe',
  subTitle: 'Specialist',
  follow: true,
  onFollow: () => {},
};

export const InvitedUserNote = Template.bind({});

InvitedUserNote.args = {
  title: 'John Doe',
  subTitle: 'Specialist',
  note: 'This is an example of a note/reason for that specific user',
  follow: true,
  onFollow: () => {},
};
