import React, { useState } from 'react';
import { TInvitedUser } from '@components/InvitedUser/InvitedUser.types';
import { Avatar, ActionButton } from '@components';
import { NotesPaperIcon } from '@icons';
import { getInitials } from '@utils';
import styles from './InvitedUser.module.scss';

const InvitedUser = ({ title, subTitle, note, follow, onFollow }: TInvitedUser) => {
  const [isFollowing, setIsFollowing] = useState<boolean>(follow);

  return (
    <div className={styles.invitedUser}>
      <div className={styles.invitedUserContent}>
        <div className={styles.invitedUserContentLeft}>
          <Avatar className={styles.invitedUserAvatar}>{getInitials(title)}</Avatar>
          <div className={styles.invitedUserTitle}>
            <h5>{title}</h5>
            {subTitle}
          </div>
        </div>
        <ActionButton
          color={isFollowing ? 'primary' : 'default'}
          size='small'
          onClick={() => {
            setIsFollowing(!isFollowing);
            onFollow();
          }}
        >
          {isFollowing ? 'Following' : 'Follow'}
        </ActionButton>
      </div>
      {note && <ActionButton icon={<NotesPaperIcon />} className={styles.invitedUserWithNote}>{note}</ActionButton>}
    </div>
  )
}

export default InvitedUser;
