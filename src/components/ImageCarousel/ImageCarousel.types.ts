export type TImageCarousel = {
  isOpen?: boolean;
  files: TSimpleFile[];
  selectedImage: number;
  onClose: () => void;
};

export type TSimpleFile = {
  file: File;
  link: string;
};
