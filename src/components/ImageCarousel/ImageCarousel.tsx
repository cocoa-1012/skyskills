import React, {useEffect, useState} from 'react';
import clsx from 'clsx';
import {ArrowRightIcon, ArrowLeftIcon, CloseIcon, DownloadIcon} from '@icons';
import {FilePreview, Flex, Modal, ActionButton} from '@components';
import {TImageCarousel} from './ImageCarousel.types';
import styles from './ImageCarousel.module.scss';

const ImageCarousel = ({isOpen, files, onClose, selectedImage}: TImageCarousel) => {
  const [currentImgIndex, setCurrentImgIndex] = useState<number>(0);
  const [modalOpened, setModalOpened] = useState<Boolean>(false);
  const [disableArrow, setDisableArrow] = useState({
    leftArrow: true,
    rightArrow: true,
  });

  useEffect(() => {
    setCurrentImgIndex(selectedImage);
  }, [selectedImage]);

  useEffect(() => {
    setModalOpened(!!isOpen);
  }, [isOpen]);

  useEffect(() => {
    if (files.length && files.length > 1) {
      if (currentImgIndex === 0) {
        setDisableArrow({...disableArrow, leftArrow: true, rightArrow: false});
      }

      else if (currentImgIndex === files.length - 1) {
        setDisableArrow({...disableArrow, leftArrow: false, rightArrow: true});
      }

      else {
        setDisableArrow({...disableArrow, leftArrow: false, rightArrow: false});
      }
    }
  }, [currentImgIndex, files.length]);

  const onArrowChange = (index: number) => {
    setCurrentImgIndex(index);
  };

  const onModalClose = () => {
    setModalOpened(false);
    onClose();
  };

  const downloadImage = (url: string, name: string) => {
    fetch(url)
      .then(resp => resp.blob())
      .then(blob => {
        const urls = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = urls;
        a.download = name;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(urls);
      });
  };

  return (
    <Modal isVisible={!!modalOpened} onCancel={onModalClose} className={styles.container}>

      {
        !disableArrow?.leftArrow && (
          <ActionButton
            size="small"
            className={clsx(styles.arrowIcon, styles.left)}
            icon={<ArrowLeftIcon/>}
            onClick={() => onArrowChange(currentImgIndex - 1)}
          />
        )
      }

      {
        !disableArrow?.rightArrow && (
          <ActionButton
            size="small"
            className={clsx(styles.arrowIcon, styles.right)}
            icon={<ArrowRightIcon/>}
            onClick={() => onArrowChange(currentImgIndex + 1)}
          />
        )
      }

      <Flex verticalCenter className={styles.header}>
        <h4>{files[currentImgIndex]?.file?.name}</h4>
        <Flex horizontalRight>

          <ActionButton
            variant="contained"
            icon={<DownloadIcon/>}
            onClick={() => {
              downloadImage(files[currentImgIndex]?.link, files[currentImgIndex]?.file?.name)
            }}
          />

          <ActionButton
            variant="contained"
            icon={<CloseIcon/>}
            onClick={onModalClose}
          />

        </Flex>
      </Flex>
      {files.length && (
        <FilePreview
          key={files[currentImgIndex]?.file?.name}
          className={styles.preview}
          type={files[currentImgIndex]?.file.type.includes('image') ? 'image' : 'pdf'}
          image={files[currentImgIndex]?.link}
          size="auto"
          detailed={false}
        />
      )}
    </Modal>
  );
};

export default ImageCarousel;
