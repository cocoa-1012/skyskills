import React, { MouseEvent } from 'react';
import clsx from 'clsx';
import { TTab } from '@components/Tabs/Tabs.types';
import styles from './Tabs.module.scss';

const Tab = ({ children, icon, isActive, onClick, onClickItem }: TTab) => {
  return (
    <button
      type='button'
      className={clsx(styles.tabItem, { [styles.tabItemActive]: isActive})}
      onClick={(event: MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        if (onClick) {
          onClick(event);
        }
        if (onClickItem) {
          onClickItem();
        }
      }}
    >
      {icon}
      {children}
    </button>
  )
}

export default Tab;
