import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { TimeOutIcon } from '@icons';
import { Tab, Tabs } from '@components'

export default {
  component: Tabs,
  title: `Components/Tabs`,
} as Meta;

const Template: Story<ComponentProps<typeof Tabs>> = args => {
  return <Tabs {...args} />;
};

export const TabsList = Template.bind({});

TabsList.args = {
  children: [<Tab>All</Tab>, <Tab icon={<TimeOutIcon />}>Scheduled</Tab>,],
};
