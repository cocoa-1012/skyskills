import { ButtonHTMLAttributes, HTMLAttributes } from 'react';

export type TTabs = HTMLAttributes<HTMLDivElement> & {
  children: JSX.Element[];
  onClickElement?: Function;
}

export type TTab = ButtonHTMLAttributes<HTMLButtonElement> & {
  children: string;
  icon?: JSX.Element;
  onClick?: Function;
  isActive?: boolean;
  onClickItem?: Function;
}
