import React, { Children, ReactElement, cloneElement, useState } from 'react';
import clsx from 'clsx';
import { TTab, TTabs } from '@components/Tabs/Tabs.types';
import styles from './Tabs.module.scss';

const Tabs = ({ children, className }: TTabs) => {
  const [activeTab, setActiveTab] = useState<number>(0);
  return (
    <div className={clsx(styles.tabList, className)}>
      {Children.map(children, (child: JSX.Element, index: number) => {
        return cloneElement(child as ReactElement<TTab>, {
          isActive: index === activeTab,
          onClick: () => setActiveTab(index),
        });
      })}
    </div>
  )
}

export default Tabs;
