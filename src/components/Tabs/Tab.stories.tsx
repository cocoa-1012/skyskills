import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import { TimeOutIcon } from '@icons';
import TabComponent from './Tab';

export default {
  component: TabComponent,
  title: `Components/Tabs/Tab`,
} as Meta;

const Template: Story<ComponentProps<typeof TabComponent>> = args => {
  return <TabComponent {...args} />;
};

export const TabDefault = Template.bind({});

TabDefault.args = {
  children: 'All',
};

export const TabIcon = Template.bind({});

TabIcon.args = {
  children: 'Scheduled',
  icon: <TimeOutIcon />,
};
