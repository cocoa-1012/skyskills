import { ReactElement } from 'react';

export type TPortal = {
  children: ReactElement;
  targetId?: string;
};
