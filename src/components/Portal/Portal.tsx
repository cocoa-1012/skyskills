import { createPortal } from 'react-dom';
import { TPortal } from './types';

const Portal = ({ children, targetId = `root` }: TPortal) => {
  const element = document.getElementById(targetId);

  return element && createPortal(children, element);
};

export default Portal;
