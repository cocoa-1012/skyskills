import React, { ChangeEvent, useState } from 'react';
import clsx from 'clsx';
import { CheckIcon } from '@icons';
import { TCheckbox } from './Checkbox.types';
import styles from './Checkbox.module.scss';

const Checkbox = ({ id, label, onChange, className, ...rest }: TCheckbox) => {
  const [isChecked, setIsChecked] = useState<boolean>(!!rest.checked);

  const onChangeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    setIsChecked(!isChecked);

    onChange?.(event);
  };

  return (
    <div className={clsx(styles.checkboxContainer, className)}>
      <label htmlFor={id}>
        <input
          className={styles.hiddenCheckbox}
          type="checkbox"
          id={id}
          checked={isChecked}
          onChange={onChangeHandler}
          {...rest}
        />
        <div
          className={clsx(styles.styledCheckbox, {
            [styles.checked]: !!isChecked,
          })}
        >
          <CheckIcon />
        </div>

        {label && <span className={styles.label}>{label}</span>}
      </label>
    </div>
  );
};

export default Checkbox;
