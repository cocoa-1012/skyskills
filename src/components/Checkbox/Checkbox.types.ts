import { ChangeEvent, InputHTMLAttributes } from 'react';

export type TCheckbox = InputHTMLAttributes<HTMLInputElement> & {
  id?: string;
  label?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
};
