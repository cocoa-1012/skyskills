import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import CheckboxComponent from './Checkbox';

export default {
  component: CheckboxComponent,
  title: `Components/Inputs/Checkbox`,
} as Meta;

const Template: Story<ComponentProps<typeof CheckboxComponent>> = args => {
  return <CheckboxComponent {...args} />;
};

export const Checkbox = Template.bind({});

Checkbox.args = {
  checked: false,
};
