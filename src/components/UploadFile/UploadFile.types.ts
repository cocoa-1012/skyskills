import { HTMLAttributes } from 'react';

export type TUploadFile = HTMLAttributes<HTMLDivElement> & {
  type?: 'image' | 'pdf';
  creator?: string;
  date?: string;
};

export type TSimpleFile = {
  file: File;
  link: string;
};

export type TFileUploadSimple = HTMLAttributes<HTMLDivElement> & {
  children: JSX.Element;
  acceptFiles?: Array<string>;
  onFileUpload: (file: TSimpleFile) => void;
};

export interface TFilePreview extends TUploadFile {
  hideImage?: boolean;
  image?: string;
  size?: 'default' | 'large' | 'auto';
  detailed?: boolean;
  onRemove?: () => void;
}
