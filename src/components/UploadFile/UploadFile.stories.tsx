import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import UploadFileComponent from './UploadFile';

export default {
  component: UploadFileComponent,
  title: `Components/UploadFile`,
} as Meta;

const Template: Story<ComponentProps<typeof UploadFileComponent>> = (arg) => {
  return <UploadFileComponent {...arg} />;
};

export const UploadFilePDF = Template.bind({});

UploadFilePDF.args = {
  type: 'pdf',
  date: 'Mon Dec 06 2021 17:09:32 GMT+0200 (Восточная Европа, стандартное время)',
}

export const UploadFileImage = Template.bind({});

UploadFileImage.args = {
  type: 'image',
  date: 'Mon Dec 03 2021 17:09:32 GMT+0200 (Восточная Европа, стандартное время)',
}
