import React, { useRef, ChangeEvent } from 'react';
import clsx from 'clsx';
import { TFileUploadSimple } from '@components/UploadFile/UploadFile.types';
import styles from './UploadFile.module.scss';

const UploadFileSimple = ({ children, acceptFiles, onFileUpload, className, ...rest }: TFileUploadSimple) => {
  const fileRef = useRef<HTMLInputElement>(null);
  const acceptedFiles = acceptFiles || ['application/pdf', 'image/*'];

  const loadFile = ({ target }: ChangeEvent<HTMLInputElement>) => {
    const file = target?.files?.[0];

    if (file) {
      onFileUpload({
        file,
        link: URL.createObjectURL(file),
      });
    }
  };

  const upload = () => {
    fileRef?.current?.click();
  };

  return (
    <div {...rest} className={clsx(styles.uploadFile, className)}>
      <div onClick={upload}>{children}</div>
      <input type="file" ref={fileRef} id="fileUpload" onChange={loadFile} accept={acceptedFiles.join(',')} />
    </div>
  );
};

export default UploadFileSimple;
