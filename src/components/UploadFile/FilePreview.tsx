import React from 'react';
import dayjs from 'dayjs';
import clsx from 'clsx';
import { TFilePreview } from '@components/UploadFile/UploadFile.types';
import { PDFIcon, CloseIcon } from '@icons';
import styles from '@components/UploadFile/UploadFile.module.scss';

const FilePreview = ({
  title,
  date,
  creator,
  image,
  type,
  size = 'default',
  hideImage,
  onRemove,
  detailed = true,
  className,
  ...rest
}: TFilePreview) => {
  return (
    <div className={clsx({ [styles.uploadFile]: !hideImage }, className)} {...rest}>
      {!hideImage && (
        <div
          className={clsx(styles.uploadFileIcon, {
            [styles.uploadFile]: !hideImage,
            [styles.auto]: size === 'auto',
            [styles.large]: size === 'large',
            [styles.default]: size === 'default',
          })}
        >
          {type === 'image' ? <img src={image} alt={title} /> : <PDFIcon />}

          {onRemove && (
            <button className={styles.removeButton} onClick={onRemove}>
              <CloseIcon />
            </button>
          )}
        </div>
      )}

      {detailed && (
        <div className={styles.uploadFileContent}>
          <p className={styles.title}>{title}</p>
          <span>
            from {dayjs(date).format('MMM D')} at {dayjs(date).format('h:mmA')} by {creator}
          </span>
        </div>
      )}
    </div>
  );
};

export default FilePreview;
