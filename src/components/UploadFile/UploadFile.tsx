import React, { useState, useRef, ChangeEvent } from 'react';
import clsx from 'clsx';
import { PDFIcon, ImageIcon } from '@icons';
import FilePreview from '@components/UploadFile/FilePreview';
import { TUploadFile } from '@components/UploadFile/UploadFile.types';
import styles from './UploadFile.module.scss';

const UploadFile = ({ creator, type = 'pdf', date, className }: TUploadFile) => {
  const [file, setFile] = useState<File | null>(null);
  const fileRef = useRef<HTMLInputElement>(null);

  const loadFile = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files !== null) {
      setFile(e.target.files[0]);
    }
  };

  const upload = () => {
    if (fileRef.current) {
      fileRef.current.click();
    }
  };

  return (
    <div className={clsx(styles.uploadFile, className)}>
      <button onClick={upload} className={styles.uploadFileIcon}>
        {type === 'pdf' && <PDFIcon />}
        {type === 'image' && !file && <ImageIcon />}
        {type === 'image' && file && <img src={URL.createObjectURL(file)} alt={file!.name} />}
      </button>
      <input
        type="file"
        ref={fileRef}
        id="fileUpload"
        onChange={loadFile}
        accept={type === 'pdf' ? 'application/pdf' : 'image/*'}
      />
      {file && <FilePreview hideImage date={date} creator={creator} title={file!.name} />}
    </div>
  );
};

export default UploadFile;
