import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import DropdownComponent from './Dropdown';

export default {
  component: DropdownComponent,
  title: `Components/Dropdown`,
} as Meta;

const Template: Story<ComponentProps<typeof DropdownComponent>> = args => <DropdownComponent {...args} />;

export const Dropdown = Template.bind({});

Dropdown.args = {
  children: <div>Trigger</div>,
  content: <div>Content</div>,
};
