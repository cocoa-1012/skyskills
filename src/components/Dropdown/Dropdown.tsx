import React, { useState, MouseEvent, useRef } from 'react';
import clsx from 'clsx';
import { useOutsideClick } from '@hooks';
import { TDropdown } from './Dropdown.types';
import styles from './Dropdown.module.scss';

const Dropdown = ({ children, content, position, className, ...rest }: TDropdown) => {
  const [isOpen, seIsOpen] = useState<boolean>(false);
  const dropdownRef = useRef(null);

  useOutsideClick(dropdownRef, () => {
    if (isOpen) {
      seIsOpen(false);
    }
  });

  return (
    <div className={styles.container}>
      <div
        aria-hidden
        className={styles.trigger}
        onClick={(event: MouseEvent) => {
          event.preventDefault();
          if (!isOpen) {
            seIsOpen(true);
          }
        }}
      >
        {children}
      </div>

      {isOpen && (
        <div
          {...rest}
          ref={dropdownRef}
          className={clsx(styles.content, className, {
            [styles.bottom]: position === 'bottom',
            [styles.bottom]: position !== 'bottom',
          })}
        >
          {content}
        </div>
      )}
    </div>
  );
};

export default Dropdown;
