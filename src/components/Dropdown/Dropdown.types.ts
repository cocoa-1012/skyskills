import { HTMLAttributes } from 'react';

export type TDropdown = HTMLAttributes<HTMLDivElement> & {
  content: JSX.Element | string;
  children?: JSX.Element | string;
  position?: 'top' | 'bottom';
};
