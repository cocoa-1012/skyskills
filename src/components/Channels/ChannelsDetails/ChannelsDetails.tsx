import React from 'react';
import clsx from 'clsx';
import { CallIcon, EmailIcon, LocalTimeIcon, CurrentLocationIcon, DeliveryTimeIcon, HomeAddressIcon } from '@icons';
import { TChannelDetails } from './ChannelsDetails.types';
import ChannelsDetailsItem from './ChannelsDetailsItem';
import styles from './ChannelsDetails.module.scss';

const ChannelsDetails = ({ channel, className }: TChannelDetails) => {
  return (
    <div className={clsx(styles.container, className)}>
      <ChannelsDetailsItem icon={<CallIcon />} title="Phone" subTitle={channel.telephone} />
      <ChannelsDetailsItem icon={<EmailIcon />} title="Email" subTitle={channel.email} />
      <ChannelsDetailsItem icon={<LocalTimeIcon />} title="Local time" subTitle={channel.localTime} />
      <ChannelsDetailsItem
        icon={<CurrentLocationIcon />}
        title="Current location"
        location={channel.currentLocation.address}
        subTitle={channel.currentLocation?.address}
      />
      <ChannelsDetailsItem icon={<DeliveryTimeIcon />} title="Delivery time" subTitle={channel.localTime} />
      <ChannelsDetailsItem icon={<HomeAddressIcon />} title="Home address" subTitle={channel?.location?.address} />
    </div>
  );
};

export default ChannelsDetails;
