import React from 'react';
import { TChannelDetailsItem } from './ChannelsDetails.types';
import styles from './ChannelsDetails.module.scss';

const ChannelsDetails = ({ title, icon, location, subTitle = 'None' }: TChannelDetailsItem) => {
  return (
    <div className={styles.detailsItem}>
      <div className={styles.detailsItemHeader}>
        {icon}
        <p>{title}</p>
      </div>
      {subTitle}
      {location && (
        <a href={location} className={styles.detailsItemLocation}>
          See on map
        </a>
      )}
    </div>
  );
};

export default ChannelsDetails;
