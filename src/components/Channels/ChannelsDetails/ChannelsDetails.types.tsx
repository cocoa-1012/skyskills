import { HTMLAttributes } from 'react';
import { TChannel } from '@sharedTypes/channel';

export type TChannelDetails = HTMLAttributes<HTMLDivElement> & {
  channel: TChannel;
  notes?: string;
};

export type TChannelDetailsItem = HTMLAttributes<HTMLDivElement> & {
  title: string;
  subTitle?: string;
  icon: JSX.Element;
  location?: string;
};
