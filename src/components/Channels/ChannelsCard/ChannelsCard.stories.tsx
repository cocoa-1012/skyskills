import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import ChannelsCardComponent from './ChannelsCard';

export default {
  component: ChannelsCardComponent,
  title: `Components/Channels/ChannelsCard`,
} as Meta;

const Template: Story<ComponentProps<typeof ChannelsCardComponent>> = args => {
  return <ChannelsCardComponent {...args} />;
};

export const ChannelsMessage = Template.bind({});

ChannelsMessage.args = {
  username: 'test',
  text: 'Test message',
  unviewed: true,
  countOfScheduled: 0,
  creationDateTime: new Date().toString(),
};
