import { HTMLAttributes } from 'react';

export type TChannelsCard = HTMLAttributes<HTMLDivElement> & {
  text: string;
  isOwner: boolean;
  isMass: boolean;
  isActive?: boolean;
  username: string;
  creationDateTime: string;
  unviewed: boolean;
  countOfScheduled: number;
};
