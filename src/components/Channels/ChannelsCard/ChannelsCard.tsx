import React from 'react';
import clsx from 'clsx';
import { dt } from '@utils';
import { TimeOutIcon } from '@icons';
import { TChannelsCard } from './ChannelsCard.types';
import styles from './ChannelsCard.module.scss';

const ChannelsCard = ({
  text,
  username,
  isMass,
  unviewed,
  creationDateTime,
  countOfScheduled,
  isOwner,
  isActive,
  className,
  ...rest
}: TChannelsCard) => {
  return (
    <div
      {...rest}
      className={clsx(styles.container, className, {
        [styles.unviewed]: unviewed && !isMass && !isOwner,
        [styles.active]: isActive,
      })}
    >
      {!isMass && <div className={styles.status} />}

      <div className={styles.messageContainer}>
        <div className={styles.titleContainer}>
          <div className={styles.title}>{username}</div>
          <div className={styles.dateTime}>{dt(creationDateTime)}</div>
        </div>

        <div className={styles.message}>
          {isOwner && 'You:'} {text}
        </div>

        {countOfScheduled > 0 && !isMass && (
          <div className={styles.scheduledContainer}>
            <TimeOutIcon />
            <p>
              {countOfScheduled} scheduled message{countOfScheduled > 1 && 's'}
            </p>
          </div>
        )}
      </div>
    </div>
  );
};

export default ChannelsCard;
