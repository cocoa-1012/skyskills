import { HTMLAttributes } from 'react';
import { TChannel } from '@sharedTypes/channel';

export type TChannelsFilter = HTMLAttributes<HTMLDivElement> & {
  variant?: 'default' | 'list';
  options?: Array<TChannel>;
  onSearched?: (value: string) => void;
  onSelected?: (value: TChannel) => void;
};
