import React, { ChangeEvent, useState } from 'react';
import clsx from 'clsx';
import { TChannelsFilter } from 'components/Channels/ChannelsFilter/ChannelsFilter.types';
import { SearchIcon, TimeOutIcon } from '@icons';
import { MenuItem, MenuList, DropdownButton, Input, Tabs, Tab, ActionButton } from '@components';
import styles from './ChannelsFilter.module.scss';

const ChannelsFilter = ({
  options,
  onSearched,
  onSelected,
  variant = 'default',
  className,
  ...rest
}: TChannelsFilter) => {
  const [isFilterOpen, setIsFilterOpen] = useState<boolean>(false);

  return (
    <div {...rest} className={clsx(styles.leftPanelContainer, className)}>
      <div className={styles.leftPanelHeader}>
        <DropdownButton
          fullWidth
          size="large"
          className={styles.leftPanelDropdown}
          placeholder={`My Drivers (${options?.length || 0})`}
        >
          <MenuList>
            {options?.length ? (
              options.map(option => (
                <MenuItem onClick={() => onSelected?.(option)} key={option.uuid}>
                  {option.name}
                </MenuItem>
              ))
            ) : (
              <p>No option filters found</p>
            )}
          </MenuList>
        </DropdownButton>

        <ActionButton
          className={clsx(styles.leftPanelFilter, { [styles.leftPanelFilterActive]: isFilterOpen })}
          size="small"
          icon={<SearchIcon />}
          onClick={() => setIsFilterOpen(!isFilterOpen)}
        />
      </div>
      {isFilterOpen && (
        <div className={styles.filterInputContainer}>
          <Input
            fullWidth
            className={styles.filterInput}
            iconLeft={<SearchIcon />}
            placeholder="Search drivers..."
            inputSize="small"
            onChange={({ target }: ChangeEvent<HTMLInputElement>) => {
              onSearched?.(target.value);
            }}
          />
        </div>
      )}
      {variant !== 'default' && (
        <Tabs className={styles.filterTabs}>
          <Tab>All</Tab>
          <Tab icon={<TimeOutIcon />}>Scheduled</Tab>
        </Tabs>
      )}
    </div>
  );
};

export default ChannelsFilter;
