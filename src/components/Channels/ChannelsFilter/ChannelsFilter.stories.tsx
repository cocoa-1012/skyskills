import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import ChannelsFilterComponent from './ChannelsFilter';

export default {
  component: ChannelsFilterComponent,
  title: `Components/Channels/ChannelsFilter`,
} as Meta;

const Template: Story<ComponentProps<typeof ChannelsFilterComponent>> = args => {
  return <ChannelsFilterComponent {...args} />;
};

export const ChannelsFilterDefault = Template.bind({});

ChannelsFilterDefault.args = {
  variant: 'default',
};

export const ChannelsFilterList = Template.bind({});

ChannelsFilterList.args = {
  variant: 'list',
};
