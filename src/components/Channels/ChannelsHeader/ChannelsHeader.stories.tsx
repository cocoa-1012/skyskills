import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import ChannelsHeaderComponent from './ChannelsHeader';

export default {
  component: ChannelsHeaderComponent,
  title: `Components/Channels/ChannelsHeader`,
} as Meta;

const Template: Story<ComponentProps<typeof ChannelsHeaderComponent>> = args => {
  return <ChannelsHeaderComponent {...args} />;
};

export const ChannelsHeader = Template.bind({});

ChannelsHeader.args = {
  username: 'John Doe',
  telephone: '(321) 722 1115',
  rate: 5,
};
