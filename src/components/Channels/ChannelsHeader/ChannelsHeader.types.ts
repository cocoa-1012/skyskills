import { HTMLAttributes } from 'react';
import { TTextTemplates } from '@components/Conversation/Chat/Chat.types';

export type TChannelsHeader = HTMLAttributes<HTMLDivElement> & {
  username: string;
  telephone: string;
  rate: number;
  textTemplates: TTextTemplates;
  onChatAdd?: () => void;
  onRateClicked?: () => void;
};
