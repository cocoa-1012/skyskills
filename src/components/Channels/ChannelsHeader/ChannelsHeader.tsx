import React, { useState } from 'react';
import clsx from 'clsx';
import { Button, ActionButton, Modal } from '@components';
import { StarIcon, NewMessageIcon } from '@icons';
import { MassMessageForm } from '@forms';
import styles from './ChannelsHeader.module.scss';
import { TChannelsHeader } from './ChannelsHeader.types';

const ChannelsHeader = ({
  username,
  telephone,
  rate,
  onChatAdd,
  onRateClicked,
  textTemplates,
  className,
  ...rest
}: TChannelsHeader) => {
  const [isMassMessagesVisible, setIsMassMessagesVisible] = useState(false);

  return (
    <div {...rest} className={clsx(styles.container, className)}>
      <div>
        <p className={styles.username}>{username}</p>
        <p className={styles.telephone}>{telephone}</p>
      </div>

      <div className={styles.controls}>
        <ActionButton size="small" icon={<StarIcon />} onClick={() => onRateClicked?.()}>
          <strong>{rate || 0}</strong>
        </ActionButton>

        <Button
          onClick={() => setIsMassMessagesVisible(true)}
          size="small"
          icon={<NewMessageIcon />}
          color="success"
          variant="contained"
        />
      </div>

      <Modal
        className={styles.massMessagesModal}
        headTitle={<h3>Send a Mass Messages</h3>}
        isVisible={isMassMessagesVisible}
        onCancel={() => setIsMassMessagesVisible(false)}
      >
        <MassMessageForm
          textTemplates={textTemplates}
          isSaving={false}
          isLoading={false}
          onError={() => {}}
          onSubmit={() => {}}
          onCancel={() => setIsMassMessagesVisible(false)}
          initialValues={{
            text: '',
            labels: [],
            follow: false,
            followTime: false,
            attachments: [],
            scheduledTime: null,
          }}
        />
      </Modal>
    </div>
  );
};

export default ChannelsHeader;
