import { HTMLAttributes } from 'react';

export type TChannelsNote = HTMLAttributes<HTMLTextAreaElement> & {
  value?: string;
};
