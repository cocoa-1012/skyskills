import React from 'react';
import clsx from 'clsx';
import { NotesPaperIcon } from '@icons';
import { TChannelsNote } from './ChannelNotes.types';
import styles from './ChannelNotes.module.scss';

const ChannelsNotes = ({ value, className, ...rest }: TChannelsNote) => {
  return (
    <div className={clsx(styles.container, className)}>
      <div className={styles.header}>
        <NotesPaperIcon /> Notes
      </div>
      <textarea {...rest} className={styles.note} value={value} />
    </div>
  );
};

export default ChannelsNotes;
