import React from 'react';
import clsx from 'clsx';
import {ChannelsCard, Loader} from '@components';
import styles from './ChannelsList.module.scss';
import {TChannelsList} from './ChannelsList.types';

const ChannelsList = ({channels, activeChannel, userUUID, onChannelSelect, isLoading, className, ...rest}: TChannelsList) => {
  console.log('channels', channels);
  return (
    <div {...rest} className={clsx(styles.channelsListContainer, className)}>
      <div className={styles.channelContainer}>
        {
          !isLoading && (channels || []).map(channel => (
            <ChannelsCard
              className={styles.channel}
              key={`channel-${channel.uuid}`}
              isActive={activeChannel?.uuid === channel.uuid}
              isOwner={userUUID === channel.lastMessage.from.uuid}
              isMass={channel.lastMessage.isMass}
              username={channel.name}
              text={channel.lastMessage.text}
              unviewed={channel.hasUnviewed}
              countOfScheduled={channel.countOfScheduled}
              creationDateTime={channel.lastMessage.creationDateTime}
              onClick={() => onChannelSelect?.(channel)}
            />
          ))
        }

        {
          !isLoading && !channels?.length && <p className={styles.noFound}>No channels found</p>
        }

        <Loader
          isVisible={isLoading}
        />

      </div>
    </div>
  );
};

export default ChannelsList;
