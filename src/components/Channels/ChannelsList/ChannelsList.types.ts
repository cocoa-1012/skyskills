import { HTMLAttributes } from 'react';
import { TChannel } from '@sharedTypes/channel';

export type TChannelsList = HTMLAttributes<HTMLDivElement> & {
  userUUID: string;
  channels: Array<TChannel>;
  isLoading: boolean;
  activeChannel?: TChannel;
  onChannelSelect?: (channel: TChannel) => void;
};
