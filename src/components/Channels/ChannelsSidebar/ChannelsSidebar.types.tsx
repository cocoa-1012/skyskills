import { HTMLAttributes } from 'react';
import { TChannel } from '@sharedTypes/channel';
import { TAttachment } from '@sharedTypes/attachment';

export type TChannelSidebar = HTMLAttributes<HTMLDivElement> & {
  channel: TChannel;
  files: Array<TAttachment>;
  notes?: string;
};
