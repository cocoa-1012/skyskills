import React, { ChangeEvent, useState } from 'react';
import clsx from 'clsx';
import { ChannelsDetails, ChannelsNotes, FilePreview, Tab, Tabs } from '@components';
import { FilesDoubleIcon, NeutralProfileIcon } from '@icons';
import { TAttachment } from '@sharedTypes/attachment';
import { TChannelSidebar } from './ChannelsSidebar.types';
import styles from './ChannelsSidebar.module.scss';

const ChannelsSidebar = ({ channel, notes, files, className }: TChannelSidebar) => {
  const [isOpenFile, setIsOpenFile] = useState<boolean>(false);

  return (
    <div className={clsx(styles.container, className)}>
      <Tabs>
        <Tab icon={<NeutralProfileIcon />} onClickItem={() => setIsOpenFile(false)}>
          Details
        </Tab>
        <Tab icon={<FilesDoubleIcon />} onClickItem={() => setIsOpenFile(true)}>
          Files
        </Tab>
      </Tabs>

      <div className={styles.contentContainer}>
        {!isOpenFile && <ChannelsDetails channel={channel} />}

        {isOpenFile && (
          <div className={styles.filesContainer}>
            {files.map((file: TAttachment) => (
              <FilePreview
                key={file.uuid}
                className={styles.filePreview}
                image={file.previewLink}
                type={file.type.includes('image') ? 'image' : 'pdf'}
                title={file.name}
                creator="You" // update with real creator
                date={file.creationDateTime}
              />
            ))}
          </div>
        )}
      </div>

      <ChannelsNotes
        value={notes}
        onChange={({ target }: ChangeEvent<HTMLTextAreaElement>) => console.log('Notes value:', target.value)}
        placeholder="You can enter some notes here (visible only to you) ..."
      />
    </div>
  );
};

export default ChannelsSidebar;
