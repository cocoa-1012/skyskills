import React from 'react';
import clsx from 'clsx';
import styles from './Avatar.module.scss';
import { TAvatar } from './Avatar.types';

const Avatar = ({ size, children, ...rest }: TAvatar) => {
  const trimValue = (value: JSX.Element | string) => {
    if (typeof value === 'string' && value.length > 2) {
      return value.slice(0, 2).toUpperCase();
    }

    return value;
  };

  return (
    <div
      {...rest}
      className={clsx(styles.avatarContainer, {
        [styles.small]: size === 'small',
        [styles.large]: size === 'large',
      })}
    >
      {trimValue(children)}
    </div>
  );
};

export default Avatar;
