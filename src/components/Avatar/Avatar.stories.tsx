import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import AvatarComponent from './Avatar';

export default {
  component: AvatarComponent,
  title: `Components/Avatar`,
} as Meta;

const Template: Story<ComponentProps<typeof AvatarComponent>> = args => {
  return <AvatarComponent {...args}>TS</AvatarComponent>;
};

export const Avatar = Template.bind({});

Avatar.args = {};
