import { HTMLAttributes } from 'react';

export type TAvatar = HTMLAttributes<HTMLDivElement> & {
  children: JSX.Element | string;
  size?: 'small' | 'large' | 'default';
};
