import React from 'react';
import clsx from 'clsx';
import { InfoIcon } from '@icons';
import { TInfo } from './Info.types';
import styles from './Info.module.scss';

const Info = ({ children, className, ...rest }: TInfo) => {
  return (
    <div {...rest} className={clsx(styles.container, className)}>
      <InfoIcon />
      <span className={styles.text}>{children}</span>
    </div>
  );
};

export default Info;
