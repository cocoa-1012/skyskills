import { HTMLAttributes } from 'react';

export type TInfo = HTMLAttributes<HTMLDivElement>;
