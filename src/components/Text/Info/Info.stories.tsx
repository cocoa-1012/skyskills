import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import TagComponent from './Info';

export default {
  component: TagComponent,
  title: `Components/Text/Info`,
} as Meta;

const Template: Story<ComponentProps<typeof TagComponent>> = args => {
  return <TagComponent {...args} />;
};

export const Info = Template.bind({});

Info.args = {
  children: 'Info text',
};
