import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import InputComponent from './InputSecure';

export default {
  component: InputComponent,
  title: `Components/Inputs/InputSecure`,
} as Meta;

const Template: Story<ComponentProps<typeof InputComponent>> = args => {
  return <InputComponent {...args} />;
};

export const InputSecure = Template.bind({});

InputSecure.args = {
  label: 'label',
  children: 'Password is strong',
};
