import React, { forwardRef, useEffect, useState } from 'react';
import clsx from 'clsx';
import { ActionButton, Flex, Input } from '@components';
import { CloseIcon, PencilAlternateIcon } from '@icons';
import { TSecureInput, TRef } from './InputSecure.types';
import styles from './InputSecure.module.scss';

const InputSecure = forwardRef(({ children, label, error, className, ...rest }: TSecureInput, ref: TRef) => {
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    if (error) {
      setIsVisible(true);
    }
  }, [error]);

  return (
    <Flex column className={clsx(styles.container, className)}>
      <div className={styles.label}>{label}</div>
      <Flex className={styles.inputContainer}>
        <Input
          {...rest}
          ref={ref}
          fullWidth
          inputSize="small"
          hidden={!isVisible}
          error={error}
          className={clsx(styles.container, {
            [styles.hidden]: !isVisible,
          })}
        />

        {!isVisible && <span className={styles.placeholder}>{children}</span>}

        {!isVisible && (
          <ActionButton
            size="small"
            className={styles.control}
            icon={<PencilAlternateIcon />}
            onClick={() => setIsVisible(!isVisible)}
          >
            Edit
          </ActionButton>
        )}

        {isVisible && (
          <ActionButton
            size="small"
            disabled={!!error}
            className={styles.control}
            icon={<CloseIcon />}
            onClick={() => {
              setIsVisible(!isVisible);
            }}
          />
        )}
      </Flex>
    </Flex>
  );
});

export default InputSecure;
