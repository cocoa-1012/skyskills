import { ForwardedRef, InputHTMLAttributes } from 'react';
import { TInputWrapper } from '@components/InputWrapper/InputWrapper.types';

export type TSecureInput = InputHTMLAttributes<HTMLInputElement> &
  Partial<TInputWrapper> & {
    onCancel?: (value: string | number) => void;
  };
export type TRef = ForwardedRef<HTMLInputElement> | null;
