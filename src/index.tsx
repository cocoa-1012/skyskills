import React from 'react';
import ReactDOM from 'react-dom';
import {RecoilRoot} from 'recoil';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import {
  Home,
  Messages,
  MassMessages,
  Drivers,
  Users,
  Settings,
  TokensPreview,
  PagesPreview,
  SignInPreview,
} from '@pages';
import { ROUTES } from '@constants';
import '@styles/index.css';
import '@styles/fonts.css';

ReactDOM.render(
  <RecoilRoot>
    <Router>
      <Routes>
        <Route path={ROUTES.index} element={<Home/>}/>
        <Route path={ROUTES.messages} element={<Messages/>}/>
        <Route path={ROUTES.massMessages} element={<MassMessages/>}/>
        <Route path={ROUTES.drivers} element={<Drivers/>}/>
        <Route path={ROUTES.users} element={<Users/>}/>
        <Route path={ROUTES.settings} element={<Settings/>}/>
        <Route path={ROUTES.development.figmaTokens} element={<TokensPreview />} />
        <Route path={ROUTES.development.figmaPages} element={<PagesPreview />} />
        <Route path={ROUTES.development.signinPage} element={<SignInPreview />} />
        <Route path="*" element={<Navigate replace to={ROUTES.index}/>}/>
      </Routes>
    </Router>
  </RecoilRoot>,
  document.getElementById('root')
);
