import React from 'react';

const MinusIcon = () => {
  return (
    <svg width="16" height="2" viewBox="0 0 16 2" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0.99999 1.99992H14.9999C15.5465 1.99992 15.9998 1.54659 15.9998 0.999929C15.9998 0.446601 15.5465 -6.10352e-05 14.9999 -6.10352e-05H0.99999C0.446662 -6.10352e-05 0 0.446601 0 0.999929C0 1.54659 0.446662 1.99992 0.99999 1.99992Z" fill="#7B93AB"/>
    </svg>
  );
}

export default MinusIcon;
