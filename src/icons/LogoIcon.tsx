import React from 'react';

const LogoIcon = () => {
  return (
    <svg width="22" height="31" viewBox="0 0 22 31" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M14.6667 15.5V0.833344L22.0001 8.16668V22.8333L14.6667 15.5Z" fill="#343442"/>
      <path d="M7.33333 22.8334V8.16669L0 15.5V30.1667H14.6667L22 22.8334H7.33333Z" fill="#343442"/>
    </svg>
  );
}

export default LogoIcon;
