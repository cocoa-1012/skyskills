import React, { ComponentProps } from 'react';
import { Meta, Story } from '@storybook/react';
import {
  FilterIcon,
  SearchIcon,
  MinusIcon,
  AlertIcon,
  ArrowDownIcon,
  CheckIcon,
  CloseIcon,
  SendEmailIcon,
  PlusIcon,
  CallIcon,
  HandUpIcon,
  ConversionScale,
  NotesPaperIcon,
  ArrowDownNotFilledIcon,
  StarActiveIcon,
  StarDefaultIcon,
  RightIcon,
  NeutralProfileIcon,
  FilesDoubleIcon,
  EmailIcon,
  LocalTimeIcon,
  CurrentLocationIcon,
  DeliveryTimeIcon,
  HomeAddressIcon,
  EditorIcon,
  GroupsIcon,
  LabelsIcon,
} from '@icons';
import styles from './Icons.module.scss';

const IconContainer = ({ icon }: any) => <div className={styles.iconsContainer}>{icon}</div>;

export default {
  "title": `Components/Icons`
} as Meta;

const Template: Story<ComponentProps<typeof IconContainer>> = (args) => <IconContainer {...args} />;

export const FilterIcons = Template.bind({});

FilterIcons.args = {
  icon: <FilterIcon />
}

export const SearchIcons = Template.bind({});

SearchIcons.args = {
  icon: <SearchIcon />
}

export const MinusIcons = Template.bind({});

MinusIcons.args = {
  icon: <MinusIcon />
}

export const AlertIcons = Template.bind({});

AlertIcons.args = {
  icon: <AlertIcon />
}

export const ArrowDownIcons = Template.bind({});

ArrowDownIcons.args = {
  icon: <ArrowDownIcon />
}

export const CheckIcons = Template.bind({});

CheckIcons.args = {
  icon: <CheckIcon />
}

export const CloseIcons = Template.bind({});

CloseIcons.args = {
  icon: <CloseIcon />
}

export const SendEmailIcons = Template.bind({});

SendEmailIcons.args = {
  icon: <SendEmailIcon />
}

export const PlusIcons = Template.bind({});

PlusIcons.args = {
  icon: <PlusIcon />
}

export const CallIcons = Template.bind({});

CallIcons.args = {
  icon: <CallIcon />
}

export const HandUpIcons = Template.bind({});

HandUpIcons.args = {
  icon: <HandUpIcon />
}

export const ConversionScales = Template.bind({});

ConversionScales.args = {
  icon: <ConversionScale />
}

export const NotesPaperIcons = Template.bind({});

NotesPaperIcons.args = {
  icon: <NotesPaperIcon />
}

export const ArrowDownNotFilledIcons = Template.bind({});

ArrowDownNotFilledIcons.args = {
  icon: <ArrowDownNotFilledIcon />
}

export const StarActiveIcons = Template.bind({});

StarActiveIcons.args = {
  icon: <StarActiveIcon />
}

export const StarDefaultIcons = Template.bind({});

StarDefaultIcons.args = {
  icon: <StarDefaultIcon />
}

export const RightIcons = Template.bind({});

RightIcons.args = {
  icon: <RightIcon />
}

export const NeutralProfileIcons = Template.bind({});

NeutralProfileIcons.args = {
  icon: <NeutralProfileIcon />
}

export const FilesDoubleIcons = Template.bind({});

FilesDoubleIcons.args = {
  icon: <FilesDoubleIcon />
}

export const EmailIcons = Template.bind({});

EmailIcons.args = {
  icon: <EmailIcon />
}

export const LocalTimeIcons = Template.bind({});

LocalTimeIcons.args = {
  icon: <LocalTimeIcon />
}

export const CurrentLocationIcons = Template.bind({});

CurrentLocationIcons.args = {
  icon: <CurrentLocationIcon />
}

export const DeliveryTimeIcons = Template.bind({});

DeliveryTimeIcons.args = {
  icon: <DeliveryTimeIcon />
}

export const HomeAddressIcons = Template.bind({});

HomeAddressIcons.args = {
  icon: <HomeAddressIcon />
}

export const EditorIcons = Template.bind({});

EditorIcons.args = {
  icon: <EditorIcon />
}

export const GroupsIcons = Template.bind({});

GroupsIcons.args = {
  icon: <GroupsIcon />
}

export const LabelsIcons = Template.bind({});

LabelsIcons.args = {
  icon: <LabelsIcon />
}
