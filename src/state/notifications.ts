import { atom } from 'recoil';

const notificationsState = atom({
  key: 'notifications',
  default: []
});

export default notificationsState;
