import {selectorFamily} from 'recoil';
import {userState} from '@state';
import {fetchChannels} from '@requests';

const channelsState = selectorFamily({
  key: 'channels',
  get: (groups?: Array<string>) => async ({get}) => {
    const {uuid} = get(userState);
    return fetchChannels(uuid, groups);
  },
});

export default channelsState;
