import { selectorFamily } from 'recoil';
import { userState } from '@state';
import { fetchMessages } from '@requests';

const messagesState = selectorFamily({
  key: 'messages',
  get: (channelUUID: string | undefined) => async ({ get }) => {

    if (channelUUID) {
      const userUUID = get(userState)?.uuid;
      return fetchMessages(userUUID, channelUUID);
    }

    return []
  },
});

export default messagesState;
