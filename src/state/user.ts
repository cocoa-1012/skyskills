import { atom } from 'recoil';
import {TUserDetailed} from "@sharedTypes/user";

type TUserState = TUserDetailed;

const userState = atom<TUserState>({
  key: 'user',
  default: {
    username: 'Joe Kohorst',
    uuid: '094596be-5695-11ec-bf63-0242ac130002',
    email: 'joe@gmail.com',
    telephone: '(359) 1234 23',
    scheduledMessages: [
      {
        uuid: '094596be-5695-11ec-bf63-0242ac930007',
        text:
          'It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.',
        isMass: false,
        creationDateTime: new Date().toString(),
        from: {
          uuid: '094596be-5695-11ec-bf63-0242ac130002',
          username: 'Joe Kohorst',
        },
      },
      {
        uuid: '094596be-5695-11ec-bf63-0242ac93000n',
        text:
          'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.',
        isMass: false,
        creationDateTime: new Date().toString(),
        from: {
          uuid: '094596be-5695-11ec-bf63-0242ac130002',
          username: 'Joe Kohorst',
        },
      },
    ],
  },
});

export default userState;
