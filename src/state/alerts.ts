import { atom } from 'recoil';

type TAlertsState = Array<string>;

const alertsState = atom<TAlertsState>({
  key: 'alerts',
  default: []
});

export default alertsState;
