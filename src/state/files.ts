import { atom } from 'recoil';
import {TAttachment} from "@sharedTypes/attachment";

type TFilesState = Array<TAttachment>;

const filesState = atom<TFilesState>({
  key: 'files',
  default: [
    {
      uuid: '0vb596be-5695-11ec-bf63-0242ac130002',
      creationDateTime: new Date().toString(),
      name: 'Image',
      previewLink: 'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg',
      downloadLink:
        'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg?cs=srgb&dl=pexels-tima-miroshnichenko-6169668.jpg&fm=jpg',
      type: 'image/jpeg',
      size: 2345234,
    },
    {
      uuid: '0vb596be-5695-11ec-bf63-0242ac130001',
      creationDateTime: new Date().toString(),
      name: 'Image',
      previewLink: 'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg',
      downloadLink:
        'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg?cs=srgb&dl=pexels-tima-miroshnichenko-6169668.jpg&fm=jpg',
      type: 'image/jpeg',
      size: 2345234,
    },
    {
      uuid: '0vb596be-5695-11ec-bf63-0242av130003',
      creationDateTime: new Date().toString(),
      name: 'Image',
      previewLink: 'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg',
      downloadLink:
        'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg?cs=srgb&dl=pexels-tima-miroshnichenko-6169668.jpg&fm=jpg',
      type: 'image/jpeg',
      size: 2345234,
    },
    {
      uuid: '0vb596be-5695-11ec-bf63-0242bc130002',
      creationDateTime: new Date().toString(),
      name: 'Image',
      previewLink: 'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg',
      downloadLink:
        'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg?cs=srgb&dl=pexels-tima-miroshnichenko-6169668.jpg&fm=jpg',
      type: 'image/jpeg',
      size: 2345234,
    },
    {
      uuid: '0vb596be-5695-11ec-bf63-0244ac130002',
      creationDateTime: new Date().toString(),
      name: 'Image',
      previewLink: 'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg',
      downloadLink:
        'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg?cs=srgb&dl=pexels-tima-miroshnichenko-6169668.jpg&fm=jpg',
      type: 'image/jpeg',
      size: 2345234,
    },
    {
      uuid: '0vb596be-5695-11ec-bf63-0562ac130002',
      creationDateTime: new Date().toString(),
      name: 'Image',
      previewLink: 'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg',
      downloadLink:
        'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg?cs=srgb&dl=pexels-tima-miroshnichenko-6169668.jpg&fm=jpg',
      type: 'image/jpeg',
      size: 2345234,
    },
    {
      uuid: '0vb596be-5695-11ec-bf63-020pac130002',
      creationDateTime: new Date().toString(),
      name: 'Image',
      previewLink: 'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg',
      downloadLink:
        'https://images.pexels.com/photos/6169668/pexels-photo-6169668.jpeg?cs=srgb&dl=pexels-tima-miroshnichenko-6169668.jpg&fm=jpg',
      type: 'image/jpeg',
      size: 2345234,
    },
  ],
});

export default filesState;
