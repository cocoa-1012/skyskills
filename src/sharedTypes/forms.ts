export type TFormData = {
  [key: string]: any;
};

export type TFormErrors = {
  [key: string]: any;
};

export interface IStandardFormProps {
  initialValues: TFormData;
  isLoading?: boolean;
  isSaving?: boolean;
  onSubmit: (data: TFormData) => void;
  onError?: (errors: TFormErrors) => void;
  onCancel?: () => void;
}
