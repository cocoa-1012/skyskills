// TODO: Will be changed as soon as it becomes clear which api for storing files will be used.

export type TAttachment = {
  uuid: string;
  name: string;
  creationDateTime: string;
  previewLink: string;
  downloadLink: string;
  size: number;
  type: string;
};
