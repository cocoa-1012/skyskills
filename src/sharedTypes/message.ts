import { TUser } from '@sharedTypes/user';
import { TAttachment } from '@sharedTypes/attachment';

export type TMessage = {
  uuid: string;
  text: string;
  attachments?: Array<TAttachment>;
  from: TUser;
  isMass: boolean;
  creationDateTime: string;
};
