import { TMessage } from '@sharedTypes/message';

export type TUser = {
  uuid: string;
  username: string;
};

export type TUserDetailed = {
  uuid: string;
  username: string;
  telephone: string;
  email: string;
  scheduledMessages: Array<TMessage>;
};
