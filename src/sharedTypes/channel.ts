import { TMessage } from './message';
import {TGroup} from './groups';

export type TChannel = {
  uuid: string;
  name: string;
  telephone: string;
  groups: Array<TGroup>,
  email: string;
  labels: Array<string>;
  localTime: string;
  deliveryTime: string;
  location: {
    address: string;
    latitude: number;
    longitude: number;
  };
  currentLocation: {
    address: string;
    latitude: number;
    longitude: number;
  };
  lastMessage: TMessage;
  hasUnviewed: boolean;
  countOfScheduled: number;
};
