const ROUTES = {
  index: '/',
  messages: '/messages',
  massMessages: '/mass-messages',
  drivers: '/drivers',
  users: '/users',
  settings: '/settings',

  development: {
    figmaTokens: '/figma-tokens',
    figmaPages: '/figma-pages',
    signinPage: '/signin',
  },
};

export default ROUTES;
