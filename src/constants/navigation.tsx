import React from 'react';
import { DoubleMessageIcon, DriversIcon, MessageIcon, SettingsIcon, UsersIcon } from '@icons';
import ROUTES from './routes';

const NAVIGATION_LINKS = {
  top: [
    {
      title: 'Messages',
      icon: <MessageIcon />,
      route: ROUTES.messages,
    },
    {
      title: 'Mass Messages',
      icon: <DoubleMessageIcon />,
      route: ROUTES.massMessages,
    },
    {
      title: 'Drivers',
      icon: <DriversIcon />,
      route: ROUTES.drivers,
    },
    {
      title: 'Users',
      icon: <UsersIcon />,
      route: ROUTES.users,
    },
  ],
  bottom: [
    {
      title: 'Settings',
      icon: <SettingsIcon />,
      route: ROUTES.settings,
    },
  ],
};

export default NAVIGATION_LINKS;
